package fileManager;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Fichero {
	/**
	 * El nombre del fichero a tratar
	 */
	private String nombreFichero;
	/**
	 * El canal de escritura con el fichero
	 */
	protected BufferedWriter ficheroW;
	/**
	 *  El canal de lectura con el fichero
	 */
	//protected BufferedReader ficheroR;
	
	/**
	 *  El canal de lectura con el fichero
	 */
	protected Scanner ficheroR;
	
	public  Fichero(String nombreFichero) {
		this.nombreFichero = nombreFichero;
		ficheroW = null;
		ficheroR = null;
	}
	/**
	 * Instancia los buffers al fichero con el nombre introducido en el constructor.<br>
	 * Si el fichero no existe, se creará vacío.<br>
	 * Se procederá a abrir el fichero para realizar operaciones sobre él.
	 */
	public void abrir() {
		File file = new File(nombreFichero);
		try {
			ficheroW = new BufferedWriter(new FileWriter(file,true));		
			//ficheroR = new BufferedReader(new FileReader(file));	
			ficheroR = new Scanner(file);
		} catch (IOException e) {
			//TODO ventana de error
		}		
		
	}
	
	/**
	 *  Se borrará el contenido del fichero
	 */
	public void vaciar() {
		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new FileWriter(nombreFichero));
			bw.write("");
			bw.close();
		} catch (Exception e) {
			//TODO ventana de error
		}
		
	}
	
	/**
	 * Al  llamar  a  este  método, se  cerrará  el  fichero  previamente  abierto.
	 */
	public void cerrar() {
		try {
			ficheroR.close();
			ficheroW.close();
		} catch (IOException e) {
			//TODO ventana de error
		}
	}
	
	public boolean existe() {
		File file = new File(nombreFichero);
		
		if (file.exists())
			return true;
		else
			return false;
		
	}
	
	

}
