package fileManager;

import java.io.IOException;
import java.util.regex.Pattern;

import logic.Usuario;

/**
 * @author vtienza
 *
 */
public class FicheroUsuario extends Fichero {

	public FicheroUsuario(String nombreFichero) {
		super(nombreFichero);
	}
	
	public void escribir(Usuario usuario) {
		
		try {
			String user = usuario.getNick() +"|"+ usuario.getPassword() +"|"+ usuario.getId();
			super.ficheroW.write(user);
			super.ficheroW.newLine();
		} catch (IOException e) {
			//TODO ventana de error
		}
		
	}
	
	public Usuario leer() throws IOException {
		String separador = Pattern.quote("|");
		String linea = null;
		if (super.ficheroR.hasNext())
			linea = super.ficheroR.nextLine();
		if (linea != null) {
			String[] argumentos = linea.split(separador);
			Usuario usuario = new Usuario(argumentos[0],argumentos[1], Integer.parseInt(argumentos[2]));
			return usuario;
		}
			return null;
	}

}
