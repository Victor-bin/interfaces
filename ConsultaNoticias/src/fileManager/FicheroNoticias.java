package fileManager;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.regex.Pattern;

import logic.Consulta;
import logic.Fuente;
import logic.Noticia;
import logic.Perodico;
/**
 * @author vtienza
 *
 */
public class FicheroNoticias extends Fichero {

	public FicheroNoticias(String nombreFichero) {
		super(nombreFichero);
	}
	
	public void escribir(int id,Consulta consulta) {
		
		try {
			//format noticia
			
			String lineaNoticia = "";
			lineaNoticia += id + "\n";
			lineaNoticia += consulta.getFechas().size() + "\n";
			for (LocalDateTime fecha : consulta.getFechas()) {
				lineaNoticia += fecha.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME) + "\n";//3804
				lineaNoticia += consulta.getNoticas(fecha).size() + "\n";
				for(Noticia noticia : consulta.getNoticas(fecha)) {
					lineaNoticia += noticia.getFuente().getNombre() + "|";
					lineaNoticia += noticia.getTitular() + "\n";
				}
			}
			super.ficheroW.write(lineaNoticia);
		} catch (IOException e) {
			//TODO ventana de error
		}
		
	}
	
	public void leer(int idUsuario, Consulta consulta) {
		//Consulta consultas = new Consulta();	
		String separador = Pattern.quote("|");
		while (super.ficheroR.hasNextInt()) {
			ArrayList<Noticia> noticias = new ArrayList<Noticia>();
			String linea = null;
			LocalDateTime fecha = null;
			int user = super.ficheroR.nextInt();
			if (user == idUsuario) {
				int numeroFechas = super.ficheroR.nextInt();
				for (int i = 0; i < numeroFechas; i++) {
					fecha = LocalDateTime.parse(super.ficheroR.next());
					int numeroNoticias = super.ficheroR.nextInt();
					super.ficheroR.nextLine();
					for (int j = 0; j < numeroNoticias; j++) {
						linea = super.ficheroR.nextLine();
						String[] argumentos = linea.split(separador);
						//JOptionPane.showMessageDialog(null, argumentos[0], "", 1); 
						noticias.add(new Noticia(new Fuente(Perodico.valueOf(argumentos[0])), argumentos[1]));
					}
				}
				consulta.addConsulta(fecha, noticias);
				//añadir a consulta
				
			} else {
				
				int numeroFechas = super.ficheroR.nextInt();
				super.ficheroR.nextLine();
				for (int i = 0; i < numeroFechas; i++) {
					super.ficheroR.nextLine();
					int numeroNoticias = super.ficheroR.nextInt();
					super.ficheroR.nextLine();
					for (int j = 0; j < numeroNoticias; j++)
						super.ficheroR.nextLine();
			}
			}	
		}
	}

}
