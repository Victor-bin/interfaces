/**
 * 
 */
package visual;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

/**
 * @author vtienza
 *
 */
public class SavedNewsPanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2056675500556071379L;
	public JScrollPane spSavedNews;
	public JTextPane tpSavedNews;
	public JButton btnSalir;
	public JButton btnVolver;
	
	public SavedNewsPanel(ActionEvents event) {
		
		spSavedNews = new JScrollPane();
		tpSavedNews = new JTextPane();
		btnVolver = new JButton("Volver");
		btnSalir = new JButton("Salir");
		
		btnVolver.addActionListener(event);
		btnSalir.addActionListener(event);
		
		GridBagLayout gbl_SavedNewsPanel = new GridBagLayout();
		gbl_SavedNewsPanel.columnWidths = new int[]{0, 0, 0};
		gbl_SavedNewsPanel.rowHeights = new int[]{0, 0, 0};
		gbl_SavedNewsPanel.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		gbl_SavedNewsPanel.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		setLayout(gbl_SavedNewsPanel);
		
		tpSavedNews.setEditable(false);
		GridBagConstraints gbc_lblNoticias_1 = new GridBagConstraints();
		gbc_lblNoticias_1.fill = GridBagConstraints.BOTH;
		gbc_lblNoticias_1.gridwidth = 2;
		gbc_lblNoticias_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNoticias_1.gridx = 0;
		gbc_lblNoticias_1.gridy = 0;
		spSavedNews.setViewportView(tpSavedNews);
		spSavedNews.setToolTipText("");
		tpSavedNews.setText("Noticias:");
		add(spSavedNews, gbc_lblNoticias_1);
		
		GridBagConstraints gbc_btnSalir = new GridBagConstraints();
		gbc_btnSalir.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnSalir.insets = new Insets(0, 0, 0, 5);
		gbc_btnSalir.gridx = 0;
		gbc_btnSalir.gridy = 1;
		add(btnSalir, gbc_btnSalir);
		
		GridBagConstraints gbc_btnVolver = new GridBagConstraints();
		gbc_btnVolver.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnVolver.gridx = 1;
		gbc_btnVolver.gridy = 1;
		add(btnVolver, gbc_btnVolver);
	}
	

}
