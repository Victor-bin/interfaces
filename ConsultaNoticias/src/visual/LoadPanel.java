/**
 * 
 */
package visual;

import java.awt.Color;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;


/**
 * @author vtienza
 *
 */
public class LoadPanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6934343211223472933L;
	
	public JProgressBar progressBar;
	public JLabel imgBreakNews;

	public LoadPanel(ActionEvents events) {
		
		progressBar = new JProgressBar();
		
		progressBar.setStringPainted(true);
		progressBar.addChangeListener(events);
		setLayout(null);
				

		progressBar.setBorderPainted(false);
		progressBar.setForeground(new Color(102, 205, 170));
		progressBar.setBounds(0, 299, 450, 28);
		progressBar.setLocation(0, 306);
		add(progressBar);
		
		imgBreakNews = new JLabel();
		imgBreakNews.setBounds(0, 0, 450, 327);
				
		File file = new File("breaking-news.jpg");
		imgBreakNews.setIcon(new ImageIcon(file.getAbsolutePath()));
		imgBreakNews.setBackground(new Color(238, 238, 238));
		add(imgBreakNews);
		events.initProgresbar(progressBar);
		
	}
}
