/**
 * 
 */
package visual;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * @author vtienza
 *
 */
public class OptionPanel extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3495030393848895745L;
	public JButton btnNoticiasActuales;
	public JButton btnConfiguracion;
	public JButton btnNoticiasGuardadas;
	public JButton btnSalir;
	
	public OptionPanel(ActionListener event) {
		
		btnNoticiasActuales = new JButton("Noticias Actuales");
		btnConfiguracion = new JButton("Configuracion");
		btnNoticiasGuardadas = new JButton("Noticias Guardadas");
		btnSalir = new JButton("Salir");

		btnNoticiasActuales.addActionListener(event);
		btnConfiguracion.addActionListener(event);
		btnNoticiasGuardadas.addActionListener(event);		
		btnSalir.addActionListener(event);
		
		GridBagConstraints gbc_optionPanel = new GridBagConstraints();
		gbc_optionPanel.fill = GridBagConstraints.BOTH;
		gbc_optionPanel.gridx = 0;
		gbc_optionPanel.gridy = 0;
	
		GridBagLayout gbl_optionPanel = new GridBagLayout();
		gbl_optionPanel.columnWidths = new int[] {0};
		gbl_optionPanel.rowHeights = new int[] {1};
		gbl_optionPanel.columnWeights = new double[]{0.0};
		gbl_optionPanel.rowWeights = new double[]{1.0, 1.0, 1.0, 1.0};
		setLayout(gbl_optionPanel);
	
		GridBagConstraints gbc_btnNoticiasActuales = new GridBagConstraints();
		gbc_btnNoticiasActuales.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnNoticiasActuales.insets = new Insets(0, 0, 5, 0);
		gbc_btnNoticiasActuales.gridx = 0;
		gbc_btnNoticiasActuales.gridy = 0;
		add(btnNoticiasActuales, gbc_btnNoticiasActuales);
		
		GridBagConstraints gbc_btnConfiguracion = new GridBagConstraints();
		gbc_btnConfiguracion.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnConfiguracion.insets = new Insets(0, 0, 5, 0);
		gbc_btnConfiguracion.gridx = 0;
		gbc_btnConfiguracion.gridy = 1;
		add(btnConfiguracion, gbc_btnConfiguracion);
	
		GridBagConstraints gbc_btnNoticiasGuardadas = new GridBagConstraints();
		gbc_btnNoticiasGuardadas.anchor = GridBagConstraints.EAST;
		gbc_btnNoticiasGuardadas.insets = new Insets(0, 0, 5, 0);
		gbc_btnNoticiasGuardadas.gridx = 0;
		gbc_btnNoticiasGuardadas.gridy = 2;
		add(btnNoticiasGuardadas, gbc_btnNoticiasGuardadas);
		
		GridBagConstraints gbc_btnSalir = new GridBagConstraints();
		gbc_btnSalir.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnSalir.gridx = 0;
		gbc_btnSalir.gridy = 3;
		add(btnSalir, gbc_btnSalir);		
	}
}