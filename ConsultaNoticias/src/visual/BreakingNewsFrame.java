package visual;

import java.awt.Image;

import javax.swing.JFrame;

import java.awt.Toolkit;
import java.awt.CardLayout;

public class BreakingNewsFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5942858382259348130L;
	public BreakingsNewsPanel breakingsNewsPanel;
	public OptionPanel optionPanel;
	public LoginPanel loginPanel;
	public LoadPanel loadPanel;
	public ConfigPanel configurationPanel;
	public SavedNewsPanel savedNewsPanel;
	private ActionEvents event;
	
	
	
	/**
	 * Launch the application.
	 */


	/**
	 * Create the application.
	 */
	public BreakingNewsFrame() {
		
		initialize();	
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		Toolkit kit = Toolkit.getDefaultToolkit();
		Image icono = kit.createImage("periodico.png");
		//Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setIconImage(icono);
		setTitle("BreakingNews");
		setResizable(false);
		setUndecorated(true);
		event = new ActionEvents(this);
		
		setBounds(100, 100, 448, 327);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(new CardLayout(0, 0));
		
		loadPanel 			= new LoadPanel(event);
		loginPanel 			= new LoginPanel(event);
		optionPanel		 	= new OptionPanel(event);
		configurationPanel 	= new ConfigPanel(event);
		breakingsNewsPanel 	= new BreakingsNewsPanel(event);
		savedNewsPanel 		= new SavedNewsPanel(event);
			
		getContentPane().add(loadPanel);
		getContentPane().add(loginPanel);
		getContentPane().add(optionPanel);
		getContentPane().add(breakingsNewsPanel);
		getContentPane().add(savedNewsPanel);
						
		loginPanel.setVisible(false);
	}
}