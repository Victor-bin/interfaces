/**
 * 
 */
package visual;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fileManager.FicheroConfiguracion;
import fileManager.FicheroNoticias;
import fileManager.FicheroUsuario;
import logic.Configuracion;
import logic.Consulta;
import logic.Fuente;
import logic.Noticia;
import logic.Perodico;
import logic.Usuario;

/**
 * @author vtienza
 *
 */
public class ActionEvents implements ActionListener, ChangeListener{
	
	private BreakingNewsFrame breakingNews;
	
	private int usuarioActual;
	
	private static String nombreFileUsr = "users.txt";
	private static String nombreFileConfig = "config.txt";
	private static String nombreFileNews = "news.txt";
	
	public ActionEvents(BreakingNewsFrame breakingNews) {
		this.breakingNews = breakingNews;
	}
	
	@Override
	public void stateChanged(ChangeEvent e) {
		
		if(e.getSource() == breakingNews.loadPanel.progressBar) 
			if (breakingNews.loadPanel.progressBar.getValue() >= 100) {						
				breakingNews.dispose();
				breakingNews.setUndecorated(false);
				breakingNews.setVisible(true);
				breakingNews.loadPanel.imgBreakNews.setVisible(false);
				cambiarPanel(breakingNews.loadPanel, breakingNews.loginPanel);
				breakingNews.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == breakingNews.optionPanel.btnSalir ||  e.getSource() == breakingNews.breakingsNewsPanel.btnSalir || e.getSource() == breakingNews.savedNewsPanel.btnSalir) 		
			salir();
		
		if (e.getSource() ==  breakingNews.loginPanel.btnIniciarSesion)
		try {
			iniciarSesion();
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(null,"FICHEROS CORUPTOS: "+e1.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
			
		
		if (e.getSource() ==  breakingNews.optionPanel.btnNoticiasActuales)
			verNoticiasActuales();
		
		if (e.getSource() == breakingNews.configurationPanel.btnVolver)
			cambiarPanel(breakingNews.configurationPanel, breakingNews.optionPanel);
		
		if (e.getSource() == breakingNews.savedNewsPanel.btnVolver)
			cambiarPanel(breakingNews.savedNewsPanel, breakingNews.optionPanel);
		
		if (e.getSource() == breakingNews.breakingsNewsPanel.btnVolver)
			cambiarPanel(breakingNews.breakingsNewsPanel, breakingNews.optionPanel);
		
		if (e.getSource() == breakingNews.optionPanel.btnConfiguracion) {
			cambiarPanel(breakingNews.optionPanel, breakingNews.configurationPanel);
			//breakingNews.setResizable(true);
			FicheroConfiguracion fileConf = new FicheroConfiguracion(nombreFileConfig);
			fileConf.abrir();
			Configuracion configuracion = fileConf.leer(usuarioActual);
			fileConf.cerrar();
			
			for (int i = 0; i < configuracion.cantidadFuentes(); i++) {
				if(configuracion.getFuente(i).getNombre().equals(Perodico.AS)) {
					breakingNews.configurationPanel.chckbxDeportes.setSelected(true);
					for(int n = 0;n < breakingNews.configurationPanel.FuentesChckbxDeportes.size();n++)
						breakingNews.configurationPanel.FuentesChckbxDeportes.get(n).setEnabled(true);
					breakingNews.configurationPanel.FuentesChckbxDeportes.get(0).setSelected(true);
				}
				
				if(configuracion.getFuente(i).getNombre().equals(Perodico.MARCA)) {
					breakingNews.configurationPanel.chckbxDeportes.setSelected(true);
					for(int n = 0;n < breakingNews.configurationPanel.FuentesChckbxDeportes.size();n++)
						breakingNews.configurationPanel.FuentesChckbxDeportes.get(n).setEnabled(true);
					breakingNews.configurationPanel.FuentesChckbxDeportes.get(1).setSelected(true);
				}
				
				if(configuracion.getFuente(i).getNombre().equals(Perodico.MUNDODEPORTIVO)) {
					breakingNews.configurationPanel.chckbxDeportes.setSelected(true);
					for(int n = 0;n < breakingNews.configurationPanel.FuentesChckbxDeportes.size();n++)
						breakingNews.configurationPanel.FuentesChckbxDeportes.get(n).setEnabled(true);
					breakingNews.configurationPanel.FuentesChckbxDeportes.get(2).setSelected(true);
				}
				
				if(configuracion.getFuente(i).getNombre().equals(Perodico.SPORT)) {
					breakingNews.configurationPanel.chckbxDeportes.setSelected(true);
					for(int n = 0;n < breakingNews.configurationPanel.FuentesChckbxDeportes.size();n++)
						breakingNews.configurationPanel.FuentesChckbxDeportes.get(n).setEnabled(true);
					breakingNews.configurationPanel.FuentesChckbxNacional.get(3).setSelected(true);
				}
				
				if(configuracion.getFuente(i).getNombre().equals(Perodico.CINCODIAS)) {
					breakingNews.configurationPanel.chckbxEconomia.setSelected(true);
					for(int n = 0;n < breakingNews.configurationPanel.FuentesChckbxEconomia.size();n++)
						breakingNews.configurationPanel.FuentesChckbxEconomia.get(n).setEnabled(true);
					breakingNews.configurationPanel.FuentesChckbxEconomia.get(0).setSelected(true);
				}
				
				if(configuracion.getFuente(i).getNombre().equals(Perodico.MEGABOLSA)) {
					breakingNews.configurationPanel.chckbxEconomia.setSelected(true);
					for(int n = 0;n < breakingNews.configurationPanel.FuentesChckbxEconomia.size();n++)
						breakingNews.configurationPanel.FuentesChckbxEconomia.get(n).setEnabled(true);
					breakingNews.configurationPanel.FuentesChckbxEconomia.get(1).setSelected(true);
				}
				
				if(configuracion.getFuente(i).getNombre().equals(Perodico.ELECONOMISTA)) {
					breakingNews.configurationPanel.chckbxEconomia.setSelected(true);
					for(int n = 0;n < breakingNews.configurationPanel.FuentesChckbxEconomia.size();n++)
						breakingNews.configurationPanel.FuentesChckbxEconomia.get(n).setEnabled(true);
					breakingNews.configurationPanel.FuentesChckbxEconomia.get(2).setSelected(true);
				}
				
				if(configuracion.getFuente(i).getNombre().equals(Perodico.EXPANSION)) {
					breakingNews.configurationPanel.chckbxEconomia.setSelected(true);
					for(int n = 0;n < breakingNews.configurationPanel.FuentesChckbxEconomia.size();n++)
						breakingNews.configurationPanel.FuentesChckbxEconomia.get(n).setEnabled(true);
					breakingNews.configurationPanel.FuentesChckbxEconomia.get(3).setSelected(true);
				}
				
				if(configuracion.getFuente(i).getNombre().equals(Perodico.BBC)) {
					breakingNews.configurationPanel.chckbxInternacional.setSelected(true);
					breakingNews.configurationPanel.FuentesChckbxInternacional.get(0).setSelected(true);
				}
				
				if(configuracion.getFuente(i).getNombre().equals(Perodico.NYTIMES)) {
					breakingNews.configurationPanel.chckbxInternacional.setSelected(true);
					for(int n = 0;n < breakingNews.configurationPanel.FuentesChckbxInternacional.size();n++)
						breakingNews.configurationPanel.FuentesChckbxInternacional.get(n).setEnabled(true);
					breakingNews.configurationPanel.FuentesChckbxInternacional.get(1).setSelected(true);
				}
				
				if(configuracion.getFuente(i).getNombre().equals(Perodico.THEGUARDIAN)) {
					breakingNews.configurationPanel.chckbxInternacional.setSelected(true);
					for(int n = 0;n < breakingNews.configurationPanel.FuentesChckbxInternacional.size();n++)
						breakingNews.configurationPanel.FuentesChckbxInternacional.get(n).setEnabled(true);
					breakingNews.configurationPanel.FuentesChckbxInternacional.get(2).setSelected(true);
				}
				
				if(configuracion.getFuente(i).getNombre().equals(Perodico.CNN)) {
					breakingNews.configurationPanel.chckbxInternacional.setSelected(true);
					for(int n = 0;n < breakingNews.configurationPanel.FuentesChckbxInternacional.size();n++)
						breakingNews.configurationPanel.FuentesChckbxInternacional.get(n).setEnabled(true);
					breakingNews.configurationPanel.FuentesChckbxInternacional.get(3).setSelected(true);
				}
				
				if(configuracion.getFuente(i).getNombre().equals(Perodico.ABC)) {
					breakingNews.configurationPanel.chckbxNacional.setSelected(true);
					for(int n = 0;n < breakingNews.configurationPanel.FuentesChckbxNacional.size();n++)
						breakingNews.configurationPanel.FuentesChckbxNacional.get(n).setEnabled(true);
					breakingNews.configurationPanel.FuentesChckbxNacional.get(0).setSelected(true);
				}
				
				if(configuracion.getFuente(i).getNombre().equals(Perodico.ELMUNDO)) {
					breakingNews.configurationPanel.chckbxNacional.setSelected(true);
					for(int n = 0;n < breakingNews.configurationPanel.FuentesChckbxNacional.size();n++)
						breakingNews.configurationPanel.FuentesChckbxNacional.get(n).setEnabled(true);
					breakingNews.configurationPanel.FuentesChckbxNacional.get(1).setSelected(true);
				}
				
				if(configuracion.getFuente(i).getNombre().equals(Perodico.LARAZON)) {
					breakingNews.configurationPanel.chckbxNacional.setSelected(true);
					for(int n = 0;n < breakingNews.configurationPanel.FuentesChckbxNacional.size();n++)
						breakingNews.configurationPanel.FuentesChckbxNacional.get(n).setEnabled(true);
					breakingNews.configurationPanel.FuentesChckbxNacional.get(2).setSelected(true);
				}
				
				if(configuracion.getFuente(i).getNombre().equals(Perodico.LAVANGUARDIA)) {
					breakingNews.configurationPanel.chckbxNacional.setSelected(true);
					for(int n = 0;n < breakingNews.configurationPanel.FuentesChckbxNacional.size();n++)
						breakingNews.configurationPanel.FuentesChckbxNacional.get(n).setEnabled(true);
					breakingNews.configurationPanel.FuentesChckbxNacional.get(3).setSelected(true);
				}
			}			
		}
		
		if (e.getSource() == breakingNews.breakingsNewsPanel.btnGuardar) 
			guardarNoticia();
		
		if (e.getSource() == breakingNews.optionPanel.btnNoticiasGuardadas)
			leerNoticias();
		
		if (e.getSource() == breakingNews.configurationPanel.chckbxDeportes) 
			habilitarChkBox(breakingNews.configurationPanel.chckbxDeportes, breakingNews.configurationPanel.FuentesChckbxDeportes);
		
		if (e.getSource() == breakingNews.configurationPanel.chckbxEconomia)
			habilitarChkBox(breakingNews.configurationPanel.chckbxEconomia, breakingNews.configurationPanel.FuentesChckbxEconomia);

		if (e.getSource() == breakingNews.configurationPanel.chckbxNacional) 
			habilitarChkBox(breakingNews.configurationPanel.chckbxNacional, breakingNews.configurationPanel.FuentesChckbxNacional);
		
		if (e.getSource() == breakingNews.configurationPanel.chckbxInternacional)
			habilitarChkBox(breakingNews.configurationPanel.chckbxInternacional, breakingNews.configurationPanel.FuentesChckbxInternacional);
		
		
		if (e.getSource() == breakingNews.configurationPanel.btnGuardar) {
				if (breakingNews.configurationPanel.chckbxDeportes.isSelected() || breakingNews.configurationPanel.chckbxEconomia.isSelected() || breakingNews.configurationPanel.chckbxInternacional.isSelected() || breakingNews.configurationPanel.chckbxNacional.isSelected()) {
					if (
						  ( breakingNews.configurationPanel.chckbxDeportes.isSelected() && 
								!(
								breakingNews.configurationPanel.FuentesChckbxDeportes.get(0).isSelected() || 
								breakingNews.configurationPanel.FuentesChckbxDeportes.get(1).isSelected() || 
								breakingNews.configurationPanel.FuentesChckbxDeportes.get(2).isSelected() || 
								breakingNews.configurationPanel.FuentesChckbxDeportes.get(3).isSelected()) 
								) ||
					      ( breakingNews.configurationPanel.chckbxEconomia.isSelected() && 
					    		 !(
					    		 breakingNews.configurationPanel.FuentesChckbxEconomia.get(0).isSelected() || 
					    		 breakingNews.configurationPanel.FuentesChckbxEconomia.get(1).isSelected() || 
					    		 breakingNews.configurationPanel.FuentesChckbxEconomia.get(2).isSelected() || 
					    		 breakingNews.configurationPanel.FuentesChckbxEconomia.get(3).isSelected()) 
					    		 ) ||
					      ( breakingNews.configurationPanel.chckbxInternacional.isSelected() && 
					    		 !(
					    		 breakingNews.configurationPanel.FuentesChckbxInternacional.get(0).isSelected() ||
					    		 breakingNews.configurationPanel.FuentesChckbxInternacional.get(1).isSelected() ||
					    		 breakingNews.configurationPanel.FuentesChckbxInternacional.get(2).isSelected() ||
					    		 breakingNews.configurationPanel.FuentesChckbxInternacional.get(3).isSelected()) 
					    		 ) ||
					      ( breakingNews.configurationPanel.chckbxNacional.isSelected() && 
					    		 !(
					    		 breakingNews.configurationPanel.FuentesChckbxNacional.get(0).isSelected() || 
					    		 breakingNews.configurationPanel.FuentesChckbxNacional.get(1).isSelected() ||
					    		 breakingNews.configurationPanel.FuentesChckbxNacional.get(2).isSelected() ||
					    		 breakingNews.configurationPanel.FuentesChckbxNacional.get(3).isSelected()) 
					    		  )
					   )
						JOptionPane.showMessageDialog(null, "Se debe marcar minimo una fuente por categoria", "Fuentes insuficiaentes", JOptionPane.WARNING_MESSAGE);
					
					else {
						FicheroConfiguracion fileConf = new FicheroConfiguracion(nombreFileConfig);
						fileConf.abrir();
						Configuracion conf = new Configuracion();
						
						if (breakingNews.configurationPanel.FuentesChckbxDeportes.get(0).isSelected()) 
							conf.addFuente(new Fuente(Perodico.AS));
						if (breakingNews.configurationPanel.FuentesChckbxDeportes.get(1).isSelected()) 
							conf.addFuente(new Fuente(Perodico.MARCA));
						if (breakingNews.configurationPanel.FuentesChckbxDeportes.get(2).isSelected()) 
							conf.addFuente(new Fuente(Perodico.MUNDODEPORTIVO));
						if (breakingNews.configurationPanel.FuentesChckbxDeportes.get(3).isSelected()) 
							conf.addFuente(new Fuente(Perodico.SPORT));
						if (breakingNews.configurationPanel.FuentesChckbxEconomia.get(0).isSelected()) 
							conf.addFuente(new Fuente(Perodico.CINCODIAS));
						if (breakingNews.configurationPanel.FuentesChckbxEconomia.get(1).isSelected()) 
							conf.addFuente(new Fuente(Perodico.MEGABOLSA));
						if (breakingNews.configurationPanel.FuentesChckbxEconomia.get(2).isSelected()) 
							conf.addFuente(new Fuente(Perodico.ELECONOMISTA));
						if (breakingNews.configurationPanel.FuentesChckbxEconomia.get(3).isSelected()) 
							conf.addFuente(new Fuente(Perodico.EXPANSION));
						if (breakingNews.configurationPanel.FuentesChckbxInternacional.get(0).isSelected()) 
							conf.addFuente(new Fuente(Perodico.BBC));
						if (breakingNews.configurationPanel.FuentesChckbxInternacional.get(1).isSelected()) 
							conf.addFuente(new Fuente(Perodico.NYTIMES));
						if (breakingNews.configurationPanel.FuentesChckbxInternacional.get(2).isSelected()) 
							conf.addFuente(new Fuente(Perodico.THEGUARDIAN));
						if (breakingNews.configurationPanel.FuentesChckbxInternacional.get(3).isSelected()) 
							conf.addFuente(new Fuente(Perodico.CNN));
						if ( breakingNews.configurationPanel.FuentesChckbxNacional.get(0).isSelected()) 
							conf.addFuente(new Fuente(Perodico.ABC));
						if ( breakingNews.configurationPanel.FuentesChckbxNacional.get(1).isSelected()) 
							conf.addFuente(new Fuente(Perodico.ELMUNDO));
						if ( breakingNews.configurationPanel.FuentesChckbxNacional.get(2).isSelected()) 
							conf.addFuente(new Fuente(Perodico.LARAZON));
						if ( breakingNews.configurationPanel.FuentesChckbxNacional.get(3).isSelected()) 
							conf.addFuente(new Fuente(Perodico.LAVANGUARDIA));
						
						fileConf.sobrescribir(usuarioActual, conf);
						breakingNews.configurationPanel.btnVolver.setEnabled(true);
						fileConf.cerrar();
						breakingNews.configurationPanel.btnGuardar.setEnabled(false);						

					}
				} else {
					JOptionPane.showMessageDialog(null, "Se debe marcar minimo una categoria", "Categorias insuficientes", JOptionPane.WARNING_MESSAGE);
				}
		}
	}
		
	
	public void initProgresbar(JProgressBar progressBar) {
		new Thread(new HiloProgressBar(nombreFileUsr, nombreFileConfig, nombreFileNews, progressBar)).start();
	}
	
	private void salir() {
		if(JOptionPane.showConfirmDialog(null, "¿Esta seguro de que desea salir?", "Salir", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
			System.exit(0);
	}

	private void iniciarSesion() {
		FicheroUsuario ficheroUsuario = new FicheroUsuario(nombreFileUsr);
		ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
		Usuario user = null;
		String nick = breakingNews.loginPanel.txtUsuario.getText();
		String password = new String(breakingNews.loginPanel.pwdContrasenya.getPassword());
		ficheroUsuario.abrir();
		try {
			user = ficheroUsuario.leer();
			while (user != null) {
				usuarios.add(user);
				user = ficheroUsuario.leer();
			} 
		} catch (IOException e1) {
			JOptionPane.showMessageDialog(null,"FICHEROS CORUPTOS: "+e1.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
		}
		for(int i = 0; i < usuarios.size(); i++)
			for(int j = i + 1; j < usuarios.size(); j++)
				if(usuarios.get(i).getPassword().equals(usuarios.get(j).getPassword()) && usuarios.get(i).getNick().equals(usuarios.get(j).getNick())) {
					JOptionPane.showMessageDialog(null, "El fichero de usuarios esta corupto: usuarios duplicados", "credenciales erroneas", JOptionPane.ERROR_MESSAGE);
					System.exit(0);
				}
					
		for (Usuario usuario : usuarios)
			if(usuario.getNick().equals(nick) && usuario.getPassword().equals(password)) {
				cambiarPanel(breakingNews.loginPanel, breakingNews.optionPanel);
				breakingNews.setJMenuBar(new Menu());
				usuarioActual = usuario.getId();
				FicheroConfiguracion conf = new FicheroConfiguracion(nombreFileConfig);
				conf.abrir();
				breakingNews.getContentPane().add(breakingNews.configurationPanel, "name_49476918433512");
				if(!conf.existeUsuario(usuarioActual)) {
					JOptionPane.showMessageDialog(null, "usuario sin configuracion", "Configuracion inicial", JOptionPane.INFORMATION_MESSAGE);
					cambiarPanel(breakingNews.optionPanel, breakingNews.configurationPanel);
					breakingNews.configurationPanel.btnVolver.setEnabled(false);
				}
				conf.cerrar();
		}
		
		if(breakingNews.loginPanel.isVisible())
			JOptionPane.showMessageDialog(null, "usuario y/o contraseña erroneos", "credenciales erroneas", JOptionPane.ERROR_MESSAGE);
		ficheroUsuario.cerrar();
	}
	
	private void verNoticiasActuales() {
		cambiarPanel(breakingNews.optionPanel, breakingNews.breakingsNewsPanel);
		breakingNews.setResizable(true);
		breakingNews.breakingsNewsPanel.tpNoticias.setText("Noticias:");
		FicheroConfiguracion fileConf = new FicheroConfiguracion(nombreFileConfig);
		fileConf.abrir();
		Configuracion conf = fileConf.leer(usuarioActual);
		for (int i = 0; i < conf.cantidadFuentes(); i++) {
			breakingNews.breakingsNewsPanel.tpNoticias.setText(breakingNews.breakingsNewsPanel.tpNoticias.getText() + "\n" + conf.getFuente(i).getCategoria().name() + " | " + conf.getFuente(i).getNombre().name() + " | " + conf.getFuente(i).obtenerNoticia().getTitular());
		}
		fileConf.cerrar();
	}
	
	private void guardarNoticia() {
		FicheroConfiguracion fileConf = new FicheroConfiguracion(nombreFileConfig);
		FicheroNoticias fileNews = new FicheroNoticias(nombreFileNews);
		fileConf.abrir();
		Configuracion conf = fileConf.leer(usuarioActual);
		ArrayList<Noticia> noticias = new ArrayList<Noticia>();
		for (int i = 0; i < conf.cantidadFuentes(); i++) {
			noticias.add(conf.getFuente(i).obtenerNoticia());
		}
		fileNews.abrir();
		Consulta consulta = new Consulta();
		consulta.addConsulta(LocalDateTime.now(), noticias);
		fileNews.escribir(usuarioActual, consulta);
		fileNews.cerrar();
		breakingNews.breakingsNewsPanel.btnGuardar.setEnabled(false);
	}
	
	public void leerNoticias(){
				breakingNews.setResizable(true);
		FicheroConfiguracion fileConf = new FicheroConfiguracion(nombreFileConfig);
		FicheroNoticias fileNews = new FicheroNoticias(nombreFileNews);
		Configuracion configuracion;
		breakingNews.savedNewsPanel.tpSavedNews.setText("(las noticias marcadas con * son las que actualmente no coinciden con la configuracion)");
		fileConf.abrir();
		configuracion = fileConf.leer(usuarioActual);
		fileConf.cerrar();
		
		fileNews.abrir();
		String noticias = "";
		Consulta consulta = new Consulta();
		fileNews.leer(usuarioActual, consulta);
		for (LocalDateTime fecha : consulta.getFechas()) {
			noticias += fecha.format(DateTimeFormatter.ISO_LOCAL_DATE) + "\n";
			for(Noticia noticia : consulta.getNoticas(fecha)) {
				if(!configuracion.existeFuente(noticia.getFuente()))
					noticias += "*";
				noticias += noticia.getFuente().getCategoria().name() + "|";
				noticias += noticia.getFuente().getNombre() + "|";
				noticias += noticia.getTitular() + "\n";
			}
		}
		fileNews.cerrar();
		breakingNews.savedNewsPanel.tpSavedNews.setText(breakingNews.savedNewsPanel.tpSavedNews.getText() + "\n" + noticias);
		cambiarPanel(breakingNews.optionPanel, breakingNews.savedNewsPanel);
	}
	
	private void habilitarChkBox(JCheckBox categoria, ArrayList<JCheckBox> fuentes) {
		if (categoria.isSelected()) {
			for (int i = 0; i < fuentes.size(); i++)
				fuentes.get(i).setEnabled(true);
		} else {
			for (int i = 0; i < fuentes.size(); i++) {
				fuentes.get(i).setEnabled(false);
				fuentes.get(i).setSelected(false);
			}
		}	
	}

	private void cambiarPanel(JPanel origen, JPanel destino) {
		origen.setVisible(false);
		destino.setVisible(true);
	}
	

}
