package logic;

import java.util.ArrayList;

public class Configuracion {
	private ArrayList<Fuente> fuentes;
	
	public Configuracion() {
		fuentes = new ArrayList<Fuente>();
	}
	
	public void addFuente(Fuente fuente) {
		fuentes.add(fuente);
	}
	
	public int cantidadFuentes() {
		return fuentes.size();		
	}
	
	public boolean existeFuente(Fuente fuente) {
		for (int i = 0; i < fuentes.size(); i++) {
			if(fuentes.get(i).getNombre() == fuente.getNombre())
				return true;
		}
			return false;
	}
	
	public Fuente getFuente(int posicion) {
		return fuentes.get(posicion);
	}
	
	public void removeFuente(Fuente fuente) {
		fuentes.remove(fuente);
	}
	
	public String toString() {
		String fuentesConfiguradas = "";
		for(int i = 0; i < fuentes.size(); i++)
			fuentesConfiguradas += fuentes.get(i).getNombre().name() + "\n";
		
		return fuentesConfiguradas;
	}
}
