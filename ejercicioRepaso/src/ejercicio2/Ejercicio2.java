package ejercicio2;

public class Ejercicio2 {
	public static void main(String[] args) {
		String name ="Victor";
		int edad = 20;
		char inicial  = 'V';
		Double altura = 1.75;
		Boolean mayorDeEdad = true;
		
		System.out.println("____ PERFIL DE ALUMNO ____");
		System.out.println("Nombre          : "+ name);
		System.out.println("Inicial         : "+ inicial);
		System.out.println("edad            : "+ edad);
		System.out.println("altura          : "+ altura);
		System.out.println("mayoria de edad : "+ mayorDeEdad);
		System.out.println("__________________________");
	}
}
