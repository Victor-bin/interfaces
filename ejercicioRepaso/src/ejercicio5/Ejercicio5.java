package ejercicio5;

import java.util.Scanner;

public class Ejercicio5 {
	
	private static Scanner leerTeclado = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		System.out.println("Defina un numero positivo N para calcular su factorial:");
		
		int n = PedirNumero();
		int factorial = factorialDeN(n);
		
		System.out.println("El factorial del numero positivo "+n+" es: "+factorial);
			

	}

	private static int PedirNumero(){
		int num;
		try {
			num = leerTeclado.nextInt();// lee la opcion
		} catch (Exception e) {
			num = PedirNumero();
		}
		
		return num;
	}
	
	private static int factorialDeN(int n){
		if (n > 1) {
			n = n * factorialDeN(n - 1);
		}
		return n;
	}
}
