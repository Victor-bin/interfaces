package ejercicio8;

import java.util.Arrays;

public class Alumno {
	/**
	 * @param dni
	 * @param nombre
	 * @param apellidos
	 * @param nacimiento
	 * @param sexo
	 * @param repetidor
	 * @param asignaturas
	 */
	public Alumno(String dni, String nombre, String apellidos, String nacimiento, Sexo sexo, boolean repetodor,
			Modulo[] asignaturas) {
		super();
		this.dni = dni;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.nacimiento = nacimiento;
		this.sexo = sexo;
		this.repetidor = repetodor;
		this.asignaturas = asignaturas;
	}
	private String dni;
	private String nombre;
	private String apellidos;
	private String nacimiento;
	private Sexo sexo;
	private boolean repetidor;
	private Modulo[] asignaturas;
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the apellidos
	 */
	public String getApellidos() {
		return apellidos;
	}
	/**
	 * @param apellidos the apellidos to set
	 */
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	/**
	 * @return the sexo
	 */
	public Sexo getSexo() {
		return sexo;
	}
	/**
	 * @param sexo the sexo to set
	 */
	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}
	/**
	 * @return the repetidor
	 */
	public boolean isRepetidor() {
		return repetidor;
	}
	/**
	 * @param repetodor the repetodor to set
	 */
	public void setRepetodor(boolean repetidor) {
		this.repetidor = repetidor;
	}
	/**
	 * @return the asignaturas
	 */
	public Modulo[] getAsignaturas() {
		return asignaturas;
	}
	/**
	 * @param asignaturas the asignaturas to set
	 */
	public void setAsignaturas(Modulo[] asignaturas) {
		this.asignaturas = asignaturas;
	}
	/**
	 * @return the dni
	 */
	public String getDni() {
		return dni;
	}
	/**
	 * @return the nacimiento
	 */
	public String getNacimiento() {
		return nacimiento;
	}
	@Override
	public String toString() {
		return "Alumno [dni=" + dni + ", nombre=" + nombre + ", apellidos=" + apellidos + ", nacimiento=" + nacimiento
				+ ", sexo=" + sexo + ", repetidor=" + repetidor + ", asignaturas=" + Arrays.toString(asignaturas) + "]";
	}
	

}
