package ejercicio8;

import java.util.Scanner;

public class Main {

	private static Scanner leerTeclado = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String titulo = 
				"DATOS COLEGIO\n"
			 + 	"_________________";
		String instruccion = 
				"Las opciones para introducir son: \n "
			+	"1-> Mostrar Alumnos\n "
			+	"2-> Mostrar Profesores\n "
			+	"3-> Mostrar Administradores\n "
			+	"4-> Mostrar Directivos\n "
			+   "0-> Salir\n";
		
		int opcion = -1;
		final int SALIR = 0;//valor de salida
		
		Profesor Manolo = new Profesor("12345678Z", "Manolo", "Fernandez Martinez", 1000.2, 7, false);
		Profesor Pablo  = new Profesor("23548789R", "Pablo", "Perez Perez", 5468.3, 7, true);
		
		Administracion Marta = new Administracion("32165498N", "Marta", "Tomelloso noso", 2650.50, "Doctorada en monetizacion", 6);
		Administracion Maria = new Administracion("89456123X", "Maria", "Robles nobles", 900.60, "Graduada en Administacion", 2);
		
		Directivo Antonio = new Directivo("6686921B", "Antonio", "orozco", 65000.2, true, Turno.Ma�ana);
		Directivo Laura = new Directivo("6686921B", "Laura", "Nivea Beani", 67000.25, false, Turno.Tarde);
		
		Modulo Matematicas = new Modulo("Matematicas", 60, Manolo, true);
		Modulo Economia = new Modulo("Introduccion a la economia", 360, Pablo, true);
		Modulo Literatura = new Modulo("Lengua castallana y literatura", 80, Pablo, false);
		Modulo Latin = new Modulo("Latin", 85, Pablo, true);
		
		Modulo[] modulos4eso = new Modulo[]{Matematicas ,Literatura};
		Modulo[] modulos4esoLetras = new Modulo[]{Matematicas ,Economia,Latin,Literatura};
		
		Alumno Erik = new Alumno("32659874G", "Erik", "Abellaneda garcia", "2020/04/66", Sexo.Hombre, false, modulos4eso );
		Alumno Alfonso = new Alumno("32659874G", "Alfonso", "Abellaneda garcia", "2020/04/66", Sexo.Hombre, false, modulos4esoLetras );

		// INICIO (TITULO Y INSTUCCIONES)
		System.out.println(titulo);
		System.out.println(instruccion);
				
		// lectura opcion
		try {
			opcion = leerTeclado.nextInt();// lee la opcion
		} catch (Exception e) {
			leerTeclado.next();
		}
		
		// bucle menu principal
		do {
			switch (opcion) {
			case 1:
				System.out.println(Erik.toString());
				System.out.println(Alfonso.toString());
			break;
			case 2:
				System.out.println(Manolo.toString());
				System.out.println(Pablo.toString());
			break;
			case 3:
				System.out.println(Marta.toString());
				System.out.println(Maria.toString());
			break;
			case 4:
				System.out.println(Antonio.toString());
				System.out.println(Laura.toString());
			break;
			default:
				System.out.println("valor invalido");
				break;
			}
			if (opcion != SALIR ) {// si no ha acabado
				System.out.println();
				System.out.println("�Desea continuar? " + instruccion);// repite las opciones
				try {
					opcion = leerTeclado.nextInt();// lee la opcion
				} catch (Exception e) {
					System.out.println("valor invalido");
					leerTeclado.next();
					opcion = 422;//resetea la opcion
				}
			} 
		} while (opcion != SALIR );// mientra la bandera este bajada
		
		
		
		
		
		
	}

}
