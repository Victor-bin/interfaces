package ejercicio8;

public class Modulo {
	/**
	 * @param nombre
	 * @param horas
	 * @param profesor
	 * @param convalidable
	 */
	public Modulo(String nombre, int horas, Profesor profesor, boolean convalidable) {
		super();
		this.nombre = nombre;
		this.horas = horas;
		this.profesor = profesor;
		this.convalidable = convalidable;
	}
	private String nombre;
	private int horas;
	private Profesor profesor;
	private boolean convalidable;
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the horas
	 */
	public int getHoras() {
		return horas;
	}
	/**
	 * @param horas the horas to set
	 */
	public void setHoras(int horas) {
		this.horas = horas;
	}
	/**
	 * @return the profesor
	 */
	public Profesor getProfesor() {
		return profesor;
	}
	/**
	 * @param profesor the profesor to set
	 */
	public void setProfesor(Profesor profesor) {
		this.profesor = profesor;
	}
	/**
	 * @return the convalidable
	 */
	public boolean isConvalidable() {
		return convalidable;
	}
	/**
	 * @param convalidable the convalidable to set
	 */
	public void setConvalidable(boolean convalidable) {
		this.convalidable = convalidable;
	}
	@Override
	public String toString() {
		return " \n Modulo [ \n  nombre=" + nombre + ", \n  horas=" + horas + ",\n  " + profesor + ",\n  convalidable="
				+ convalidable + "]";
	}

}
