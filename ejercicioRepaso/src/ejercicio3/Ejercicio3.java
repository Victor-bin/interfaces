package ejercicio3;

public class Ejercicio3 {

	public static void main(String[] args) {
		int[] primosHastaCien = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
		
		for (int i = 0; i < primosHastaCien.length; i++) {
			if (primosHastaCien[i] % 2 == 0) {
				System.out.println("EL "+primosHastaCien[i]+" ES PRIMO, PAR Y MENOR DE CIEN");
			}
		}

	}

}
