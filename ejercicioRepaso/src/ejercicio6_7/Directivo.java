package ejercicio6_7;

public class Directivo extends Trabajador {
	/**
	 * @param dni
	 * @param nombre
	 * @param apellidos
	 * @param salario
	 * @param salesiano
	 * @param turno
	 */
	public Directivo(String dni, String nombre, String apellidos, Double salario, boolean salesiano, Turno turno) {
		super(dni, nombre, apellidos, salario);
		this.setSalesiano(salesiano);
		this.setTurno(turno);
	}
	private boolean salesiano;
	private Turno turno;
	/**
	 * @return the salesiano
	 */
	public boolean isSalesiano() {
		return salesiano;
	}
	/**
	 * @param salesiano the salesiano to set
	 */
	public void setSalesiano(boolean salesiano) {
		this.salesiano = salesiano;
	}
	/**
	 * @return the turno
	 */
	public Turno getTurno() {
		return turno;
	}
	/**
	 * @param turno the turno to set
	 */
	public void setTurno(Turno turno) {
		this.turno = turno;
	}
	@Override
	public String toString() {
		return "Directivo [ "+ super.toString() +", salesiano=" + salesiano + ", turno=" + turno + "]";
	}
	

}
