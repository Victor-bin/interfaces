package ejercicio6_7;

/**
 * @author victor
 *
 */
public class Profesor extends Trabajador {
	/**
	 * @param dni
	 * @param nombre
	 * @param apellidos
	 * @param salario
	 * @param numeroAsignaturas
	 * @param tutor
	 */
	public Profesor(String dni, String nombre, String apellidos, Double salario, int numeroAsignaturas,
			boolean tutor) {
		super(dni, nombre, apellidos, salario);
		this.numeroAsignaturas = numeroAsignaturas;
		this.tutor = tutor;
	}
	private int numeroAsignaturas;
	private boolean tutor;
	/**
	 * @return the numeroAsignaturas
	 */
	public int getNumeroAsignaturas() {
		return numeroAsignaturas;
	}
	/**
	 * @param numeroAsignaturas the numeroAsignaturas to set
	 */
	public void setNumeroAsignaturas(int numeroAsignaturas) {
		this.numeroAsignaturas = numeroAsignaturas;
	}
	/**
	 * @return the tutor
	 */
	public boolean isTutor() {
		return tutor;
	}
	/**
	 * @param tutor the tutor to set
	 */
	public void setTutor(boolean tutor) {
		this.tutor = tutor;
	}
	@Override
	public String toString() {
		return "Profesor [ "+ super.toString() + ", numeroAsignaturas=" + numeroAsignaturas + ", tutor=" + tutor + " ]";
	}

}
