package ejercicio6_7;


public class Administracion extends Trabajador {
	/**
	 * @param dni
	 * @param nombre
	 * @param apellidos
	 * @param salario
	 * @param estudios
	 * @param antiguedad
	 */
	public Administracion(String dni, String nombre, String apellidos, Double salario, String estudios,
			int antiguedad) {
		super(dni, nombre, apellidos, salario);
		this.setEstudios(estudios);
		this.setAntiguedad(antiguedad);
	}
	private String estudios;
	private int antiguedad;
	
	/**
	 * @return the estudios
	 */
	public String getEstudios() {
		return estudios;
	}
	/**
	 * @param estudios the estudios to set
	 */
	public void setEstudios(String estudios) {
		this.estudios = estudios;
	}
	/**
	 * @return the antiguedad
	 */
	public int getAntiguedad() {
		return antiguedad;
	}
	/**
	 * @param antiguedad the antiguedad to set
	 */
	public void setAntiguedad(int antiguedad) {
		this.antiguedad = antiguedad;
	}
	@Override
	public String toString() {
		return "Administracion [ "+ super.toString() +", estudios=" + estudios + ", antiguedad=" + antiguedad + "]";
	}
	
	
	
}
