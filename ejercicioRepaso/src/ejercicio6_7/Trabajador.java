package ejercicio6_7;

/**
 * @author victor
 *
 */
public class Trabajador {
	/**
	 * @param dni
	 * @param nombre
	 * @param apellidos
	 * @param salario
	 */
	public Trabajador(String dni, String nombre, String apellidos, Double salario) {
		super();
		this.dni = dni;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.salario = salario;
	}
	private String dni;
	private String nombre;
	private String apellidos;
	private Double salario;
	/**
	 * @return the dni
	 */
	public String getDni() {
		return dni;
	}
	/**
	 * @param dni the dni to set
	 */
	public void setDni(String dni) {
		this.dni = dni;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the apellidos
	 */
	public String getApellidos() {
		return apellidos;
	}
	/**
	 * @param apellidos the apellidos to set
	 */
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	/**
	 * @return the salario
	 */
	public Double getSalario() {
		return salario;
	}
	/**
	 * @param salario the salario to set
	 */
	public void setSalario(Double salario) {
		this.salario = salario;
	}
	@Override
	public String toString() {
		return " DNI=" + dni + ", nombre=" + nombre + ", apellidos=" + apellidos + ", salario=" + salario;
	}
}
