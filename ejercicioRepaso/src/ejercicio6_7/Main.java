package ejercicio6_7;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Profesor Manolo = new Profesor("12345678Z", "Manolo", "Fernandez Martinez", 1000.2, 7, false);
		Profesor Pablo  = new Profesor("23548789R", "Pablo", "Perez Perez", 5468.3, 7, true);
		
		Administracion Marta = new Administracion("32165498N", "Marta", "Tomelloso noso", 2650.50, "Doctorada en monetizacion", 6);
		Administracion Maria = new Administracion("89456123X", "Maria", "Robles nobles", 900.60, "Graduada en Administacion", 2);
		
		Directivo Antonio = new Directivo("6686921B", "Antonio", "orozco", 65000.2, true, Turno.Ma�ana);
		Directivo Laura = new Directivo("6686921B", "Laura", "Nivea Beani", 67000.25, false, Turno.Tarde);

		System.out.println(Manolo.toString());
		System.out.println(Pablo.toString());
		System.out.println(Marta.toString());
		System.out.println(Maria.toString());
		System.out.println(Antonio.toString());
		System.out.println(Laura.toString());
	}

}
