package ejercicio4;

import java.util.Scanner;

public class Ejercicio4 {
	
	private static Scanner leerTeclado = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		System.out.println("Defina un numero N para el tama�o de la matriz N * N");
		
		int n = 0;
		int suma = 0;
			n = leerTeclado.nextInt();

		int[][] matriz = new int[n][n];
		
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz.length; j++) {
				matriz[i][j] = (int)(Math.random()*10);
				suma += matriz[i][j];
			}
		}
		
		System.out.println("Tu matriz de " + n + " * " + n + " con numeros del cero al nueve : ");
		
		for (int i = 0; i < matriz.length; i++) {
			System.out.println();
			for (int j = 0; j < matriz.length; j++) {
				System.out.print(" " + matriz[i][j]);
			}
		}
		System.out.println();
		System.out.println("Cuya suma es: " + suma);
		
		
	}
}
