# Tema 3
## Ejercicio 1
### Construir un archivo xml, que incluya un catálogo de películas (cartelera). 
Los elementos que puede considerar son: 
- Título (obligatorio)
- idioma
- duración
- horarios (obligatorio)
- clasificación de la película

Añadir otros elementos o atributos que consideréisimportantes.(obligatorio)

## Ejercicio 2
### Construir un archivo xml, que respete las siguientes consideraciones:.
Los puntos que puede considerar son: 
- Los motores están compuestos de piezas que a su vez pueden componerse de otras.
- Nunca un tipo de motor puede ser fabricado por más de una sección, ni esta puede cambiar una vez se le ha asignado la fabricación de un determinado motor.
- Entre los empleados que trabajan en un tipo de motor siempre habrá un responsable, siendo esta información de interés para la empresa. Un empleado puede estar trabajando en más de un tipo de motor.
- Los empleados pueden tener familiares a su cargo.

# Entrega
## Ejercicio 1
Un fabricante de bicicletas eléctricas desea estandarizar el formato XML de sus proveedores y que cumplan las siguientes condiciones:
- Un pedido consta de uno o más bicicletas.
- Una bicicleta eléctrica consta de uno o más componentes.
- Un componente tiene los siguientes elementos: nombre del fabricante (obligatorio), fecha de entrega (si es posible, si aparece el día es optativo, pero el mes y el año no). También se necesita saber del componente si es frágil o no. Debe aparecer un elemento peso del componente y dicho elemento peso tiene un atributo unidad del peso (kilos o gramos), 
- Uno de esos elementos será la batería que posee las siguientes características: número de serie y puede que aparezca o no un elemento “ciclos_batería” indicando que el componente debe sustituirse tras un cierto número de ciclos/recargas.

## Ejercicio 2
Un colegio necesita registrar los cursos y alumnos/as que estudian en él y necesita documentoXML estandarizado para que todos los tutores sepan cómo deben registrar los datos:
- Tiene que haber un elemento raízlistacursos. Tiene que haber uno o más cursos.
- Un curso tiene uno o más alumnos/as.
- Todo alumno/as tiene un DNI, un nombre y un apellido, puede que tenga segundo apellido o no.
- Un alumno/as escoge una lista de asignaturas donde habrá una o más asignaturas. Toda asignatura tiene un nombre, un código y un profesor.
