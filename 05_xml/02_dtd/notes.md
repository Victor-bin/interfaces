# DTD
## Símbolos

- *NADA* una única vez
- ***\**** Opcional y repetible (0 o más veces)
- ***?*** Opcional (0 ó 1 vez)
- ***+*** Necesario y repetible (1 ó más veces)
- ***|*** Opcional entre dos elementos

## Elementos
- ***(\#PCDATA)*** cadena de caracteres

## Atributos
Se definen con ***<!ATTLIST Elemento NomAtr Tipo Valpred>***

- ***CDATA*** String

- ***\#REQUIRED*** Obligatorio
- ***\#IMPLIED*** Opcional
- ***\#FIXED*** Valor fijo

### EJEMPLOS
#### Etiqueta:
```xml
<! DOCTYPE etiqueta [
    <! ELEMENT etiqueta (nombre, calle, ciudad, pais, codigo)>
    <!ELEMENT nombre (#PCDATA)>
    <!ELEMENT calle (#PCDATA)>
    <!ELEMENT ciudad (#PCDATA)>
    <!ELEMENT pais (#PCDATA)>
    <!ELEMENT codigo (#PCDATA)>
]>
<etiqueta>
    <nombre> Fulanito De Tal </nombre>
    <calle> C/Mayor, 1 </calle>
    <ciudad> Madrid </ciudad>
    <pais> España </pais>
    <codigo> 123456 </codigo>
</etiqueta>
```

#### Libro DTD
```xml
<!ELEMENT libreria3 ( libro+ ) >
<!ELEMENT libro ( autor+, titulo,
precio,rebaja? ) >
<!ELEMENT autor ( #PCDATA ) >
<!ELEMENT titulo ( #PCDATA ) >
<!ELEMENT precio ( #PCDATA ) >
<!ELEMENT rebaja ( #PCDATA ) >
```

#### Libro XML:
```xml
<libro>
    <autor>Miguel De Cervantes</autor>
    <titulo>El Quijote</titulo>
    <precio>18</precio>
</libro>
<libro>
    <autor>Antonio Lopez</autor>
    <autor>Ana Perez</autor>
    <titulo>Cómo crecer</titulo>
    <precio>25</precio>
    <rebaja>10%</rebaja>
</libro>
```
