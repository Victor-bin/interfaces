package noticeWindow;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;

public class Window {

	private JFrame frmTraductor;
	private JTextField jtfTitular;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window window = new Window();
					window.frmTraductor.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Window() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTraductor = new JFrame();
		frmTraductor.setFont(new Font("SansSerif", Font.PLAIN, 13));
		frmTraductor.setTitle("NOVEDADES INSTITUTO");
		frmTraductor.setBounds(100, 100, 450, 300);
		frmTraductor.setLocationRelativeTo(null);
		frmTraductor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTraductor.getContentPane().setLayout(null);
		
		JLabel lblTitulo = new JLabel("ULTIMA NOTICIA");
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setFont(new Font("Noto Sans", Font.PLAIN, 13));
		lblTitulo.setBounds(26, 43, 402, 25);
		frmTraductor.getContentPane().add(lblTitulo);
		
		jtfTitular = new JTextField();
		jtfTitular.setFont(new Font("Dialog", Font.BOLD, 12));
		jtfTitular.setHorizontalAlignment(SwingConstants.CENTER);
		jtfTitular.setEditable(false);
		jtfTitular.setBounds(36, 114, 392, 62);
		frmTraductor.getContentPane().add(jtfTitular);
		jtfTitular.setColumns(10);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//jtfTitular.setText("Buscando...");
				String titular = buscarTitular();
				// FIRST CHAR UPPERCASE
				titular = Character.toUpperCase(titular.charAt(0)) + titular.substring(1);
				jtfTitular.setText(titular);
				
			}

			private String buscarTitular() {
				Document document;
				String webPage = "https://www.salesianosdosa.com/category/noticias/";
				try {
					document = Jsoup.connect(webPage).get();
					Elements palabra = document.getElementsByClass("post-title entry-title");
					return palabra.get(0).text();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return "No se pudo realizar la conexion";
				}
				
				
			}
		});
		btnBuscar.setToolTipText("Pulsa para traducir la palabra situada en la parte superior");
		btnBuscar.setBounds(36, 211, 98, 25);
		frmTraductor.getContentPane().add(btnBuscar);
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jtfTitular.setText("");
			}
		});
		btnBorrar.setBounds(326, 211, 98, 25);
		frmTraductor.getContentPane().add(btnBorrar);
		
		JLabel lblImagen = new JLabel("");
		//lblImagen.setIcon(new ImageIcon("logodosa.png"));
		lblImagen.setIcon(new ImageIcon(Window.class.getResource("/noticeWindow/logodosa.png")));
		lblImagen.setBounds(326, 0, 83, 78);
		frmTraductor.getContentPane().add(lblImagen);
	}
}
