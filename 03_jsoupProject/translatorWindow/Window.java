package translatorWindow;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;

public class Window {

	private JFrame frmTraductor;
	private final JTextField palabraIntroducida = new JTextField();
	private JTextField palabraTraducida;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window window = new Window();
					window.frmTraductor.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Window() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTraductor = new JFrame();
		frmTraductor.setFont(new Font("SansSerif", Font.PLAIN, 13));
		frmTraductor.setTitle("TRADUCTOR");
		frmTraductor.setBounds(100, 100, 450, 300);
		frmTraductor.setLocationRelativeTo(null);
		frmTraductor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTraductor.getContentPane().setLayout(null);
		
		JLabel lblIntroduzcaLaPalabra = new JLabel("Introduzca la palabra a traducir:");
		lblIntroduzcaLaPalabra.setFont(new Font("Noto Sans", Font.PLAIN, 13));
		lblIntroduzcaLaPalabra.setBounds(36, 32, 192, 15);
		frmTraductor.getContentPane().add(lblIntroduzcaLaPalabra);
		palabraIntroducida.setBounds(234, 22, 153, 35);
		frmTraductor.getContentPane().add(palabraIntroducida);
		palabraIntroducida.setColumns(10);
		
		JLabel lblPalabraTraducida = new JLabel("Palabra traducida:");
		lblPalabraTraducida.setFont(new Font("Noto Sans", Font.PLAIN, 13));
		lblPalabraTraducida.setBounds(36, 174, 153, 15);
		frmTraductor.getContentPane().add(lblPalabraTraducida);
		
		palabraTraducida = new JTextField();
		palabraTraducida.setEditable(false);
		palabraTraducida.setBounds(234, 164, 153, 35);
		frmTraductor.getContentPane().add(palabraTraducida);
		palabraTraducida.setColumns(10);
		
		JButton btnTraducir = new JButton("Traducir");
		btnTraducir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(palabraIntroducida.getText().isEmpty()) {
					JOptionPane.showMessageDialog(frmTraductor, " NECESITA INTRODUCIR UNA PALABRA PARA TRADUCIRLA", "PALABRA VACIA", JOptionPane.INFORMATION_MESSAGE);
				} else {
					String palabra = traducirPalabra(palabraIntroducida.getText());
					// FIRST CHAR UPPERCASE
					palabra = Character.toUpperCase(palabra.charAt(0)) + palabra.substring(1);
					palabraTraducida.setText(palabra);
				}
				
			}

			private String traducirPalabra(String text) {
				Document document;
				String webPage = "https://www.spanishdict.com/traductor/"+text;
				
				try {
					document = Jsoup.connect(webPage).get();
					Elements palabra = document.getElementById("quickdef1-es").getElementsByClass("_1btShz4h");
					return palabra.html();
				} catch (IOException e1) {
					e1.printStackTrace();
					return "Palabra no encontrada";
				}
				
			}
		});
		btnTraducir.setToolTipText("Pulsa para traducir la palabra situada en la parte superior");
		btnTraducir.setBounds(177, 97, 98, 25);
		frmTraductor.getContentPane().add(btnTraducir);
	}
}
