/**
 * 
 */
package translator;

import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements; 
/**
 * @author vtienza
 *
 */
public class Main {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		Document document;
		String webPage = "https://www.spanishdict.com/traductor/manzana";
		document = Jsoup.connect(webPage).get();
		Elements palabra = document.getElementById("quickdef1-es").getElementsByClass("_1btShz4h");
		System.out.println("PALABRA -> "+ palabra.html());
		
		Document document2;
		String webPage2 = "https://www.salesianosdosa.com/category/noticias/";
		document2 = Jsoup.connect(webPage2).get();
		Elements palabra2 = document2.getElementsByClass("post-title entry-title");
		System.out.println("Titular -> "+ palabra2.get(0).text());
	}
}
