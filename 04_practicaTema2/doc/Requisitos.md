# Descripción
Desarrollar una herramienta para consultar las noticias.

# Consideración
- Al intentar salir independientemente de la ventana nos debe salir un mensaje de confirmación.
- NO se permite NINGUNA traza.
- Icono no predeterminado.
- Ventana centrada, Considerar tamaño y maximizar y minimizar.
- Barra de menú con "Acerca de" con datos del creador y información de aplicación. (derecha). una vez iniciada sesión.

# Archivos necesarios 
- *usuarios y contraseñas* (creados externamente) (.txt) (3 usuarios) puede haber dos nombres iguales con distintas contraseñas (¿ID?).
- configuración. La configuración va relacionada con su usuario y las titulares.
- log de titulares. Los titulares van relacionadas con su usuario. Las noticias no se sobrescriben al volver a guardar, se añaden. incluye fecha y hora de guardado.

# Ventana de Carga
- Imagen de fondo. imagen fija relacionada con noticias.
- Barra de carga (5 segundos).
- Comprobación de los Archivos. si falta alguno la aplicación no se ejecuta (ruta relativas)
- El botón de salir "X" esta deshabilitado.

# Ventana de Login
- Usuario y contraseña (contraseña oculta)
- Si el login es correcto va a opciones.
- Si no es correcto se muestra mensajes, si lo es va opciones (si es la primera vez puede ir a configuración)
- Botón de login.

# Ventana de opciones
1. Ver titulares actuales -> Ventana mostrar titulares (solo si existe configuración)
1. (EXTRA, FILTRAR POR DÍAS) (EXTRA PERSONAL: Filtrar por categorías) (FILTRO POR PAGINA)
1. Configuración -> Ventana de configuración
1. Salir -> Pedir confirmación

# Ventana mostrar titulares actuales
- NO botón de actualizar 
- Ver titulares 
- Botones de "Volver a menú", "salir" y "guardar noticias" (solo una vez por sesión)
- Muestra categoría, fuente y titular.

# Ventana mostrar titulares guardados
- Botones de "Volver a menú", "salir"
- Si no existen noticias salta aviso de información.
- Ver titulares guardados (EXTRA, FILTRAR POR DIAS) (EXTRA PERSONAL: Filtrar por categorías) (FILTRO POR PAGINA) (EXTRA PERSONAL:contador de noticias )
- Muestra categoría, fuente y titular. (FECHA)
- Muestra Todas las noticias, indicando las que la configuración ya no mostraría. (Mostrar de mas antigua a la nueva) (LO BONITO SERIA AL REVÉS)

# Ventana de configuración
- cada usuario selecciona una serie de categorías (Deportes, Nacional, Economía, Internacional). Mínimo 1, máximo 4.
- cada usuario fuentes de información. 5 por categoría. Mínimo 1, máximo 5. (1 titular de cada fuente). Fuentes a elegir.
- de esta ventana se va a titulares o a opciones?
- la configuración de puede sobrescribir. solo se guarda una vez por sesión (la nueva configuración se aplica en el momento)
- Botones : volver, guardar.

# Estructura de código
- Visual (4 a 6 archivos)
- Lógica
- Acceso datos
