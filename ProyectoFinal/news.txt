1
1
2020-11-18T18:16:22.24
16
AS|El 'Top-10' de las victorias de España
MARCA|Eric García, inalcanzable para el Barça; el Manchester City no rebaja un euro
MUNDODEPORTIVO|Las 10 razones de Messi para ser feliz en la selección argentina
SPORT|Suárez vuelve a dar positivo y no jugará ante el Barça
CINCODIAS|Banca Genç (BBVA) avisa de que la fusión con Sabadell solo se hará si crea realmente valor Á.G. Madrid
MEGABOLSA|Análisis BBVA, Solaria, Gamesa y Acerinox
ELECONOMISTA|Los expertos proponen una jubilación flexible entre los 60 y los 75 años y que el trabajador elija
EXPANSION|Iberdrola busca sitio para una gran fábrica española de máquinas de hidrógeno
BBC|"Es un momento alarmante": el experto que compara la situación de EE.UU. con la crisis y caída de la República romana
NYTIMES|Brasil disminuye los contagios de COVID-19 y la popularidad de Bolsonaro aumenta
THEGUARDIAN|Brutal Covid second wave exposes Italy's shortage of intensive care staff
CNN|Desestiman cargos contra Salvador Cienfuegos
ABC|Sanidad deja la puerta abierta a que las farmacias puedan realizar test como piden Madrid y Cataluña
ELMUNDO|Bruselas señala a España como el país con menos ayudas en 2021 frente a la pandemia
LARAZON|Iglesias pide a Sánchez apoyar al Sáhara en plena crisis migratoria
LAVANGUARDIA|Bruselas avala el presupuesto de España, pero advierte sobre la elevada deuda
1
1
2020-11-19T00:13:06.767364
4
ABC|La consultora de Podemos desvió 307.000 euros a México con ocho transferencias en dos meses
ELMUNDO|PP, Vox y Cs se unen a JxCat y PNV para "garantizar" una oferta "suficiente" en la concertada
LARAZON|Covid-19
LAVANGUARDIA|Illa pide un plan “detallado” a las autonomías que quieren que las farmacias hagan test de antígenos
