/**
 * 
 */
package visual;

import java.awt.EventQueue;

import javax.swing.JOptionPane;

/**
 * @author vtienza
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BreakingNewsFrame window = new BreakingNewsFrame();
					window.setVisible(true);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Error "+e.getMessage(), "Error interno", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
	}

}
