/**
 * 
 */
package visual;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;


import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;


/**
 * @author vtienza
 *
 */
public class LoginPanel extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public JTextField txtUsuario;
	public JPasswordField pwdContrasenya;
	public JButton btnIniciarSesion;
	
	public LoginPanel(ActionListener event) {

		GridBagLayout gbl_loginPanel = new GridBagLayout();
		gbl_loginPanel.columnWidths = new int[]{225, 225, 0};
		gbl_loginPanel.rowHeights = new int[]{90, 90, 90, 0};
		gbl_loginPanel.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		gbl_loginPanel.rowWeights = new double[]{1.0, 1.0, 1.0, Double.MIN_VALUE};
		setLayout(gbl_loginPanel);
		
		JLabel lblUsuario = new JLabel("USUARIO :");
		lblUsuario.setHorizontalAlignment(SwingConstants.TRAILING);
		GridBagConstraints gbc_lblUsuario = new GridBagConstraints();
		gbc_lblUsuario.fill = GridBagConstraints.BOTH;
		gbc_lblUsuario.insets = new Insets(0, 0, 5, 5);
		gbc_lblUsuario.gridx = 0;
		gbc_lblUsuario.gridy = 0;
		add(lblUsuario, gbc_lblUsuario);
		
		txtUsuario = new JTextField();
		txtUsuario.setMinimumSize(new Dimension(125, 19));
		txtUsuario.setColumns(100);
		GridBagConstraints gbc_txtUsuario = new GridBagConstraints();
		gbc_txtUsuario.anchor = GridBagConstraints.WEST;
		gbc_txtUsuario.insets = new Insets(0, 0, 5, 0);
		gbc_txtUsuario.gridx = 1;
		gbc_txtUsuario.gridy = 0;
		add(txtUsuario, gbc_txtUsuario);
		
		JLabel lblContrasea = new JLabel("CONTRASEÑA :");
		lblContrasea.setHorizontalAlignment(SwingConstants.TRAILING);
		GridBagConstraints gbc_lblContrasea = new GridBagConstraints();
		gbc_lblContrasea.fill = GridBagConstraints.BOTH;
		gbc_lblContrasea.insets = new Insets(0, 0, 5, 5);
		gbc_lblContrasea.gridx = 0;
		gbc_lblContrasea.gridy = 1;
		add(lblContrasea, gbc_lblContrasea);
		
		pwdContrasenya = new JPasswordField();
		pwdContrasenya.setMinimumSize(new Dimension(125, 19));
		pwdContrasenya.setColumns(10);
		GridBagConstraints gbc_pwdContrasenya = new GridBagConstraints();
		gbc_pwdContrasenya.anchor = GridBagConstraints.WEST;
		gbc_pwdContrasenya.insets = new Insets(0, 0, 5, 0);
		gbc_pwdContrasenya.gridx = 1;
		gbc_pwdContrasenya.gridy = 1;
		add(pwdContrasenya, gbc_pwdContrasenya);
		
		btnIniciarSesion = new JButton("Iniciar sesion");
		GridBagConstraints gbc_btnIniciarSesion = new GridBagConstraints();
		gbc_btnIniciarSesion.gridwidth = 2;
		gbc_btnIniciarSesion.insets = new Insets(0, 0, 0, 5);
		gbc_btnIniciarSesion.gridx = 0;
		gbc_btnIniciarSesion.gridy = 2;
		
		btnIniciarSesion.addActionListener(event);
		add(btnIniciarSesion, gbc_btnIniciarSesion);
		
	}

}
