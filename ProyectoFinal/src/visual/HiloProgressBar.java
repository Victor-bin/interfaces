/**
 * 
 */
package visual;

import javax.swing.JOptionPane;
import javax.swing.JProgressBar;

import fileManager.FicheroConfiguracion;
import fileManager.FicheroNoticias;
import fileManager.FicheroUsuario;

/**
 * @author vtienza
 *
 */
public class HiloProgressBar implements Runnable {
	
	private String nombreFileUsr;
	private String nombreFileConfig;
	private String nombreFileNews;
	private JProgressBar progressBar;
	
	public HiloProgressBar(String nombreFileUsr,String nombreFileConfig,String nombreFileNews, JProgressBar progressBar) {
		this.nombreFileUsr = nombreFileUsr;
		this.nombreFileConfig = nombreFileConfig;
		this.nombreFileNews = nombreFileNews;
		this.progressBar = progressBar;
	}
	
	@Override
	public void run() {
		
		FicheroUsuario ficheroUsuario = new FicheroUsuario(nombreFileUsr);
		FicheroConfiguracion ficheroConfg = new FicheroConfiguracion(nombreFileConfig);
		FicheroNoticias ficheroNoticias = new FicheroNoticias(nombreFileNews);
		
		for (int i = 0; i <= 100; i++) {
			
			if (i == 0) {
				String progreso = "Comprobando usuarios...";
				progressBar.setString(progreso);
				if (!ficheroUsuario.existe()) {
					JOptionPane.showMessageDialog(null, "El Fichero " + nombreFileUsr + " no existe.", "Fichero no encontrado", JOptionPane.ERROR_MESSAGE);	
				}
			}
			
			if (i == 40) {
				String progreso = "Comprobando configuracion...";
				progressBar.setString(progreso);
				if (!ficheroConfg.existe()) {
					JOptionPane.showMessageDialog(null, "El Fichero " + nombreFileConfig + " no existe.", "Fichero no encontrado", JOptionPane.ERROR_MESSAGE);
					System.exit(0);
				}
			}
			
			if (i == 70) {
				String progreso = "Comprobando noticias...";
				progressBar.setString(progreso);
				if (!ficheroNoticias.existe()) {
					JOptionPane.showMessageDialog(null, "El Fichero " + nombreFileNews + " no existe.", "Fichero no encontrado", JOptionPane.ERROR_MESSAGE);
					System.exit(0);
				}
			}
							
			progressBar.setValue(i);
			progressBar.repaint();
			
			try {
				Thread.sleep(50);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		
	}
	
}
