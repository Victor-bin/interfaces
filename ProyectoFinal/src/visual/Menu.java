/**
 * 
 */
package visual;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

/**
 * @author vtienza
 *
 */
public class Menu  extends JMenuBar{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6015143923200937375L;

	public Menu() {
		//JMenuBar menuBar = new JMenuBar();
		setBounds(0, 0, 437, 25);
		add(Box.createHorizontalGlue());
		
		
		JMenu mnAcercaDe = new JMenu("...");
		add(mnAcercaDe);
		
		JMenuItem mntmAcercaDe = new JMenuItem("Acerca de");
		mntmAcercaDe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Victor Tienza, 2020 \n version: 1.0", "Desarrollado por", 1);
			}
		});
		mnAcercaDe.add(mntmAcercaDe);
	}

	
}
