package visual;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;


/**
 * @author vtienza
 *
 */
public class BreakingsNewsPanel extends JPanel{
	
	private static final long serialVersionUID = -6232179710335329684L;
	public JScrollPane spNoticias;
	public JTextPane tpNoticias;
	public JButton btnGuardar;
	public JButton btnVolver;
	public JButton btnSalir;
	
	public BreakingsNewsPanel(ActionEvents event) {
		
		spNoticias = new JScrollPane();
		tpNoticias = new JTextPane();
		btnSalir = new JButton("Salir");
		btnGuardar = new JButton("Guardar");
		btnVolver = new JButton("Volver");
		btnGuardar.addActionListener(event);
		btnVolver.addActionListener(event);
		btnSalir.addActionListener(event);
		
		GridBagLayout gbl_BreakingsNewsPanel = new GridBagLayout();
		gbl_BreakingsNewsPanel.columnWidths = new int[]{0, 0, 0, 0};
		gbl_BreakingsNewsPanel.rowHeights = new int[]{0, 0, 0};
		gbl_BreakingsNewsPanel.columnWeights = new double[]{1.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_BreakingsNewsPanel.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		setLayout(gbl_BreakingsNewsPanel);
		
		tpNoticias.setEditable(false);
		spNoticias.setViewportView(tpNoticias);
		tpNoticias.setToolTipText("");
		tpNoticias.setText("Noticias:");
		GridBagConstraints gbc_tpNoticias = new GridBagConstraints();
		gbc_tpNoticias.fill = GridBagConstraints.BOTH;
		gbc_tpNoticias.gridwidth = 3;
		gbc_tpNoticias.insets = new Insets(0, 0, 5, 0);
		gbc_tpNoticias.gridx = 0;
		gbc_tpNoticias.gridy = 0;
		add(spNoticias, gbc_tpNoticias);
		
		GridBagConstraints gbc_btnSalir_1 = new GridBagConstraints();
		gbc_btnSalir_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnSalir_1.insets = new Insets(0, 0, 0, 5);
		gbc_btnSalir_1.gridx = 0;
		gbc_btnSalir_1.gridy = 1;
		add(btnSalir, gbc_btnSalir_1);
		GridBagConstraints gbc_btnGuardar = new GridBagConstraints();
		gbc_btnGuardar.insets = new Insets(0, 0, 0, 5);
		gbc_btnGuardar.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnGuardar.gridx = 1;
		gbc_btnGuardar.gridy = 1;
		add(btnGuardar, gbc_btnGuardar);
		GridBagConstraints gbc_btnVolver = new GridBagConstraints();
		gbc_btnVolver.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnVolver.gridx = 2;
		gbc_btnVolver.gridy = 1;
		add(btnVolver, gbc_btnVolver);
	}
}