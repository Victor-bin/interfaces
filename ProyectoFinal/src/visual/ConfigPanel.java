/**
 * 
 */
package visual;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;


/**
 * @author vtienza
 *
 */
public class ConfigPanel extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 732406715136089908L;
	
	public JCheckBox chckbxDeportes;
	public JCheckBox chckbxEconomia;
	public JCheckBox chckbxNacional;
	public JCheckBox chckbxInternacional;
	
	public ArrayList<JCheckBox> FuentesChckbxDeportes;
	public ArrayList<JCheckBox> FuentesChckbxEconomia;
	public ArrayList<JCheckBox> FuentesChckbxNacional;
	public ArrayList<JCheckBox> FuentesChckbxInternacional;
	
	public JButton btnVolver;
	public JButton btnGuardar;
	
	public ConfigPanel(ActionEvents event)  {
		
		FuentesChckbxDeportes = new ArrayList<JCheckBox>();
		FuentesChckbxEconomia = new ArrayList<JCheckBox>();
		FuentesChckbxNacional = new ArrayList<JCheckBox>();
		FuentesChckbxInternacional = new ArrayList<JCheckBox>();
		
		chckbxDeportes = new JCheckBox("Deportes");
		JCheckBox chckbxSPORTDeportes = new JCheckBox("sport");
		JCheckBox chckbxMARCADeportes = new JCheckBox("marca");
		JCheckBox chckbxASDeportes = new JCheckBox("as");
		JCheckBox chckbxMUNDODEPORTIVODeportes = new JCheckBox("mundo deportivo");
		FuentesChckbxDeportes.add(chckbxASDeportes);
		FuentesChckbxDeportes.add(chckbxMARCADeportes);
		FuentesChckbxDeportes.add(chckbxMUNDODEPORTIVODeportes);
		FuentesChckbxDeportes.add(chckbxSPORTDeportes);

		chckbxEconomia = new JCheckBox("Economía");
		JCheckBox chckbxELECONOMISTAEconomia = new JCheckBox("el economista");
		JCheckBox chckbxEXPANSIONEconomia = new JCheckBox("expansion");
		JCheckBox chckbxMEGABOLSAEconomia = new JCheckBox("megabolsa");
		JCheckBox chckbxCINCODIASEconomia = new JCheckBox("cinco dias");
		FuentesChckbxEconomia.add(chckbxCINCODIASEconomia);
		FuentesChckbxEconomia.add(chckbxELECONOMISTAEconomia);
		FuentesChckbxEconomia.add(chckbxMEGABOLSAEconomia);
		FuentesChckbxEconomia.add(chckbxEXPANSIONEconomia);

		chckbxNacional = new JCheckBox("Nacional");
		JCheckBox chckbxABCNacional = new JCheckBox("abc");
		JCheckBox chckbxLARAZONNacional = new JCheckBox("la razon");
		JCheckBox chckbxELMUNDONacional = new JCheckBox("el mundo");
		JCheckBox chckbxLAVANGUARDIANacional = new JCheckBox("la vanguardia");
		FuentesChckbxNacional.add(chckbxABCNacional);
		FuentesChckbxNacional.add(chckbxELMUNDONacional);
		FuentesChckbxNacional.add(chckbxLARAZONNacional);
		FuentesChckbxNacional.add(chckbxLAVANGUARDIANacional);

		chckbxInternacional = new JCheckBox("Internacional");
		JCheckBox chckbxBBCInternacional = new JCheckBox("bbc");
		JCheckBox chckbxNYTIMESInternacional = new JCheckBox("new york times");
		JCheckBox chckbxTHEGUARDIANInternacional = new JCheckBox("the guardian");
		JCheckBox chckbxCNNInternacional = new JCheckBox("cnn");
		FuentesChckbxInternacional.add(chckbxBBCInternacional);
		FuentesChckbxInternacional.add(chckbxNYTIMESInternacional);
		FuentesChckbxInternacional.add(chckbxTHEGUARDIANInternacional);
		FuentesChckbxInternacional.add(chckbxCNNInternacional);
		
		chckbxDeportes.addActionListener(event);
		chckbxEconomia.addActionListener(event);
		chckbxInternacional.addActionListener(event);
		chckbxNacional.addActionListener(event);
		
		btnGuardar = new JButton("Guardar");
		btnVolver = new JButton("Volver");
		
		btnGuardar.addActionListener(event);
		btnVolver.addActionListener(event);
		
		JLabel lblCategorias = new JLabel("Categorias:");
		
		
		GridBagLayout gbl_ConfigurationPanel = new GridBagLayout();
		gbl_ConfigurationPanel.columnWidths = new int[] {0, 0, 0, 0};
		gbl_ConfigurationPanel.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_ConfigurationPanel.columnWeights = new double[]{1.0, 1.0, 1.0, 1.0};
		gbl_ConfigurationPanel.rowWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0};
		setLayout(gbl_ConfigurationPanel);
		
		
		GridBagConstraints gbc_lblCategorias = new GridBagConstraints();
		gbc_lblCategorias.gridwidth = 4;
		gbc_lblCategorias.insets = new Insets(0, 0, 5, 0);
		gbc_lblCategorias.gridx = 0;
		gbc_lblCategorias.gridy = 0;
		add(lblCategorias, gbc_lblCategorias);

		GridBagConstraints gbc_chckbxDeportes = new GridBagConstraints();
		gbc_chckbxDeportes.gridwidth = 4;
		gbc_chckbxDeportes.fill = GridBagConstraints.VERTICAL;
		gbc_chckbxDeportes.insets = new Insets(0, 0, 5, 0);
		gbc_chckbxDeportes.gridx = 0;
		gbc_chckbxDeportes.gridy = 1;
		add(chckbxDeportes, gbc_chckbxDeportes);
		
		chckbxSPORTDeportes.setEnabled(false);
		chckbxSPORTDeportes.setActionCommand("SPORT");
		GridBagConstraints gbc_chckbxSPORTDeportes = new GridBagConstraints();
		gbc_chckbxSPORTDeportes.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxSPORTDeportes.gridx = 0;
		gbc_chckbxSPORTDeportes.gridy = 2;
		add(chckbxSPORTDeportes, gbc_chckbxSPORTDeportes);
		
		chckbxMARCADeportes.setEnabled(false);
		GridBagConstraints gbc_chckbxMARCADeportes = new GridBagConstraints();
		gbc_chckbxMARCADeportes.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxMARCADeportes.gridx = 1;
		gbc_chckbxMARCADeportes.gridy = 2;
		add(chckbxMARCADeportes, gbc_chckbxMARCADeportes);
		
		chckbxASDeportes.setEnabled(false);
		GridBagConstraints gbc_chckbxASDeportes = new GridBagConstraints();
		gbc_chckbxASDeportes.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxASDeportes.gridx = 2;
		gbc_chckbxASDeportes.gridy = 2;
		add(chckbxASDeportes, gbc_chckbxASDeportes);
		
		chckbxMUNDODEPORTIVODeportes.setEnabled(false);
		GridBagConstraints gbc_chckbxMUNDODEPORTIVODeportes = new GridBagConstraints();
		gbc_chckbxMUNDODEPORTIVODeportes.insets = new Insets(0, 0, 5, 0);
		gbc_chckbxMUNDODEPORTIVODeportes.gridx = 3;
		gbc_chckbxMUNDODEPORTIVODeportes.gridy = 2;
		add(chckbxMUNDODEPORTIVODeportes, gbc_chckbxMUNDODEPORTIVODeportes);
		
		chckbxELECONOMISTAEconomia.setEnabled(false);
		chckbxELECONOMISTAEconomia.setActionCommand("Fuente1");
		GridBagConstraints gbc_chckbxELECONOMISTAEconomia = new GridBagConstraints();
		gbc_chckbxELECONOMISTAEconomia.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxELECONOMISTAEconomia.gridx = 0;
		gbc_chckbxELECONOMISTAEconomia.gridy = 4;
		add(chckbxELECONOMISTAEconomia, gbc_chckbxELECONOMISTAEconomia);
		
		chckbxEXPANSIONEconomia.setEnabled(false);
		GridBagConstraints gbc_chckbxEXPANSIONEconomia = new GridBagConstraints();
		gbc_chckbxEXPANSIONEconomia.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxEXPANSIONEconomia.gridx = 1;
		gbc_chckbxEXPANSIONEconomia.gridy = 4;
		add(chckbxEXPANSIONEconomia, gbc_chckbxEXPANSIONEconomia);
		
		chckbxMEGABOLSAEconomia.setEnabled(false);
		GridBagConstraints gbc_chckbxMEGABOLSAEconomia = new GridBagConstraints();
		gbc_chckbxMEGABOLSAEconomia.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxMEGABOLSAEconomia.gridx = 2;
		gbc_chckbxMEGABOLSAEconomia.gridy = 4;
		add(chckbxMEGABOLSAEconomia, gbc_chckbxMEGABOLSAEconomia);
		
		chckbxCINCODIASEconomia.setEnabled(false);
		GridBagConstraints gbc_chckbxCINCODIASEconomia = new GridBagConstraints();
		gbc_chckbxCINCODIASEconomia.insets = new Insets(0, 0, 5, 0);
		gbc_chckbxCINCODIASEconomia.gridx = 3;
		gbc_chckbxCINCODIASEconomia.gridy = 4;
		add(chckbxCINCODIASEconomia, gbc_chckbxCINCODIASEconomia);
		
		GridBagConstraints gbc_chckbxEconomia = new GridBagConstraints();
		gbc_chckbxEconomia.gridwidth = 4;
		gbc_chckbxEconomia.fill = GridBagConstraints.VERTICAL;
		gbc_chckbxEconomia.insets = new Insets(0, 0, 5, 0);
		gbc_chckbxEconomia.gridx = 0;
		gbc_chckbxEconomia.gridy = 3;
		add(chckbxEconomia, gbc_chckbxEconomia);
		
		chckbxABCNacional.setEnabled(false);
		chckbxABCNacional.setActionCommand("ABC");
		GridBagConstraints gbc_chckbxABCNacional = new GridBagConstraints();
		gbc_chckbxABCNacional.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxABCNacional.gridx = 0;
		gbc_chckbxABCNacional.gridy = 6;
		add(chckbxABCNacional, gbc_chckbxABCNacional);
		
		chckbxLARAZONNacional.setEnabled(false);
		GridBagConstraints gbc_chckbxLARAZONNacional = new GridBagConstraints();
		gbc_chckbxLARAZONNacional.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxLARAZONNacional.gridx = 1;
		gbc_chckbxLARAZONNacional.gridy = 6;
		add(chckbxLARAZONNacional, gbc_chckbxLARAZONNacional);
		
		chckbxELMUNDONacional.setEnabled(false);
		GridBagConstraints gbc_chckbxELMUNDONacional = new GridBagConstraints();
		gbc_chckbxELMUNDONacional.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxELMUNDONacional.gridx = 2;
		gbc_chckbxELMUNDONacional.gridy = 6;
		add(chckbxELMUNDONacional, gbc_chckbxELMUNDONacional);
		
		chckbxLAVANGUARDIANacional.setEnabled(false);
		GridBagConstraints gbc_chckbxLAVANGUARDIANacional = new GridBagConstraints();
		gbc_chckbxLAVANGUARDIANacional.insets = new Insets(0, 0, 5, 0);
		gbc_chckbxLAVANGUARDIANacional.gridx = 3;
		gbc_chckbxLAVANGUARDIANacional.gridy = 6;
		add(chckbxLAVANGUARDIANacional, gbc_chckbxLAVANGUARDIANacional);

		GridBagConstraints gbc_chckbxNacional = new GridBagConstraints();
		gbc_chckbxNacional.gridwidth = 4;
		gbc_chckbxNacional.fill = GridBagConstraints.VERTICAL;
		gbc_chckbxNacional.insets = new Insets(0, 0, 5, 0);
		gbc_chckbxNacional.gridx = 0;
		gbc_chckbxNacional.gridy = 5;
		add(chckbxNacional, gbc_chckbxNacional);

		GridBagConstraints gbc_chckbxInternacional = new GridBagConstraints();
		gbc_chckbxInternacional.gridwidth = 4;
		gbc_chckbxInternacional.fill = GridBagConstraints.VERTICAL;
		gbc_chckbxInternacional.insets = new Insets(0, 0, 5, 0);
		gbc_chckbxInternacional.gridx = 0;
		gbc_chckbxInternacional.gridy = 7;
		add(chckbxInternacional, gbc_chckbxInternacional);

		chckbxBBCInternacional.setEnabled(false);
		chckbxBBCInternacional.setActionCommand("Fuente1");
		GridBagConstraints gbc_chckbxBBCInternacional = new GridBagConstraints();
		gbc_chckbxBBCInternacional.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxBBCInternacional.gridx = 0;
		gbc_chckbxBBCInternacional.gridy = 8;
		add(chckbxBBCInternacional, gbc_chckbxBBCInternacional);
		
		chckbxNYTIMESInternacional.setEnabled(false);
		GridBagConstraints gbc_chckbxNYTIMESInternacional = new GridBagConstraints();
		gbc_chckbxNYTIMESInternacional.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxNYTIMESInternacional.gridx = 1;
		gbc_chckbxNYTIMESInternacional.gridy = 8;
		add(chckbxNYTIMESInternacional, gbc_chckbxNYTIMESInternacional);
		
		chckbxTHEGUARDIANInternacional.setEnabled(false);
		GridBagConstraints gbc_chckbxTHEGUARDIANInternacional = new GridBagConstraints();
		gbc_chckbxTHEGUARDIANInternacional.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxTHEGUARDIANInternacional.gridx = 2;
		gbc_chckbxTHEGUARDIANInternacional.gridy = 8;
		add(chckbxTHEGUARDIANInternacional, gbc_chckbxTHEGUARDIANInternacional);
		
		chckbxCNNInternacional.setEnabled(false);
		GridBagConstraints gbc_chckbxCNNInternacional = new GridBagConstraints();
		gbc_chckbxCNNInternacional.insets = new Insets(0, 0, 5, 0);
		gbc_chckbxCNNInternacional.gridx = 3;
		gbc_chckbxCNNInternacional.gridy = 8;
		add(chckbxCNNInternacional, gbc_chckbxCNNInternacional);

		GridBagConstraints gbc_btnGuardar = new GridBagConstraints();
		gbc_btnGuardar.fill = GridBagConstraints.BOTH;
		gbc_btnGuardar.gridwidth = 2;
		gbc_btnGuardar.insets = new Insets(0, 0, 0, 5);
		gbc_btnGuardar.gridx = 0;
		gbc_btnGuardar.gridy = 9;
		add(btnGuardar, gbc_btnGuardar);
		
		GridBagConstraints gbc_btnVolver = new GridBagConstraints();
		gbc_btnVolver.fill = GridBagConstraints.BOTH;
		gbc_btnVolver.gridwidth = 2;
		gbc_btnVolver.gridx = 2;
		gbc_btnVolver.gridy = 9;
		add(btnVolver, gbc_btnVolver);			
	}
}