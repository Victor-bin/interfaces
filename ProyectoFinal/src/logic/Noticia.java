package logic;

public class Noticia {
	private Fuente fuente;
	private String titular;
	
	public Noticia(Fuente fuente, String titular) {
		this.fuente = fuente;
		this.titular = titular;
	}
	
	/**
	 * @return the fuente
	 */
	public Fuente getFuente() {
		return fuente;
	}
	/**
	 * @return the titular
	 */
	public String getTitular() {
		return titular;
	}
	

}
