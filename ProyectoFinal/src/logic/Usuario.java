package logic;

public class Usuario {
	private String nick;
	private String password;
	private int id;
	private Configuracion config;
	private Consulta listaNoticias;
	
	public Usuario(String nick, String password, int id) {
		this.nick = nick;
		this.password = password;
		this.id = id;
		listaNoticias = new Consulta();
	}

	/**
	 * @return the nick
	 */
	public String getNick() {
		return nick;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the config
	 */
	public Configuracion getConfig() {
		return config;
	}

	/**
	 * @param config the config to set
	 */
	public void setConfig(Configuracion config) {
		this.config = config;
	}
	

	public Consulta getConsulta() {
		return listaNoticias;
	}
	
	/**
	 * @param listaNoticias the listaNoticias to set
	 */
	public void setConsulta(Consulta consulta) {
		listaNoticias = consulta;
	}

	@Override
	public String toString() {
		return "Usuario [nick=" + nick + ", password=" + password + ", id=" + id + ", config=" + config
				+ ", listaNoticias=" + listaNoticias + "]";
	}
	
	
}
