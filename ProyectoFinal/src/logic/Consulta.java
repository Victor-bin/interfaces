package logic;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;

public class Consulta {
	private HashMap<LocalDateTime, ArrayList<Noticia>> consulta;
	
	public Consulta() {
		consulta =  new HashMap<LocalDateTime, ArrayList<Noticia>>();
	}
	
	public Consulta(LocalDateTime hora, ArrayList<Noticia> noticias) {
		consulta =  new HashMap<LocalDateTime, ArrayList<Noticia>>();
		addConsulta(hora, noticias);
	}
	
	public void addConsulta (LocalDateTime fecha, ArrayList<Noticia> noticias) {
		consulta.put(fecha, noticias);
	}
	
	public void removeConsulta (LocalDateTime fecha, ArrayList<Noticia> noticias) {
		consulta.remove(fecha, noticias);
	}
	
	public ArrayList<LocalDateTime> getFechas() {
		ArrayList<LocalDateTime> fechas = new ArrayList<LocalDateTime>();
		for (LocalDateTime fecha : consulta.keySet()) 
			fechas.add(fecha);
		return fechas;
	}
	
	public ArrayList<Noticia> getNoticas(LocalDateTime fecha) {
		return consulta.get(fecha);
	}
	
	
	public String toString() {
		String noticias = "";
		//for(int i = 0; i < consultas.size(); i++)
			//noticias += consultas. + "\n";
		for (LocalDateTime fecha : consulta.keySet()) {
			noticias += fecha + consulta.get(fecha).toString() +"\n";
		}
		
		return noticias;
	}
}