package logic;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Fuente {
	private Categoria categoria;
	private Perodico nombre;
	
	public Fuente (Perodico nombre) {
		if(nombre == Perodico.AS || nombre == Perodico.MUNDODEPORTIVO || nombre == Perodico.MARCA || nombre == Perodico.SPORT)
			categoria = Categoria.DEPORTES;
		if(nombre == Perodico.ABC || nombre == Perodico.LARAZON || nombre == Perodico.ELMUNDO || nombre == Perodico.LAVANGUARDIA)
			categoria = Categoria.NACIONAL;
		if(nombre == Perodico.EXPANSION || nombre == Perodico.ELECONOMISTA || nombre == Perodico.MEGABOLSA || nombre == Perodico.CINCODIAS)
			categoria = Categoria.ECONOMIA;
		if(nombre == Perodico.BBC || nombre == Perodico.NYTIMES || nombre == Perodico.THEGUARDIAN || nombre == Perodico.CNN)
			categoria = Categoria.INTERNACIONAL;
		
		this.nombre = nombre;
	}
	
	/**
	 * @return the categoria
	 */
	public Categoria getCategoria() {
		return categoria;
	}



	/**
	 * @return the nombre
	 */
	public Perodico getNombre() {
		return nombre;
	}



	public Noticia obtenerNoticia() {
		String titular = "ERROR NOTICIA NO ENCOTRADA";
		if (categoria == Categoria.DEPORTES) {
			if (nombre == Perodico.AS)
				titular = getAS();
			if (nombre == Perodico.MUNDODEPORTIVO)
				titular = getMUNDODEPORTIVO();
			if (nombre == Perodico.MARCA)
				titular = getMARCA();
			if (nombre == Perodico.SPORT)
				titular = getSPORT();
		}
		
		if (categoria == Categoria.ECONOMIA) {
			if (nombre == Perodico.ELECONOMISTA)
				titular = getELECONOMISTA();
			if (nombre == Perodico.EXPANSION)
				titular = getEXPANSION();
			if (nombre == Perodico.MEGABOLSA)
				titular = getMEGABOLSA();
			if (nombre == Perodico.CINCODIAS)
				titular = getCINCODIAS();
		}
		
		if (categoria == Categoria.INTERNACIONAL) {
			if (nombre == Perodico.BBC)
				titular = getBBC();
			if (nombre == Perodico.NYTIMES)
				titular = getNYTIMES();
			if (nombre == Perodico.THEGUARDIAN)
				titular = getTHEGUARDIAN();
			if (nombre == Perodico.CNN)
				titular = getCNN();
		}

		if (categoria == Categoria.NACIONAL) {
			if (nombre == Perodico.ABC)
				titular = getABC();
			if (nombre == Perodico.LARAZON)
				titular = getLARAZON();
			if (nombre == Perodico.ELMUNDO)
				titular = getELMUNDO();
			if (nombre == Perodico.LAVANGUARDIA)
				titular = getLAVANGUARDIA();
		}
		Noticia noticia = new Noticia(this,titular);
		
		return noticia;
			
	}
	
	
	private String getLAVANGUARDIA() {
		Document document;
		String webPage = "https://www.lavanguardia.com/";
		try {
			document = Jsoup.connect(webPage).get();
			Elements palabra = document.getElementsByClass("story-header-title-link");
			return palabra.get(0).text();
		} catch (IOException e) {
			return "No se pudo realizar la conexion con "+webPage;
		}
	}

	private String getELMUNDO() {
		Document document;
		String webPage = "https://www.elmundo.es";
		try {
			document = Jsoup.connect(webPage).get();
			Elements palabra = document.getElementsByClass("ue-c-cover-content__headline");
			return palabra.get(0).text();
		} catch (IOException e) {
			return "No se pudo realizar la conexion con "+webPage;
		}
	}

	private String getLARAZON() {
		Document document;
		String webPage = "https://www.larazon.es";
		try {
			document = Jsoup.connect(webPage).get();
			Elements palabra = document.getElementsByClass("opening").get(0).getElementsByClass("card__headline");
			return palabra.get(0).text();
		} catch (IOException e) {
			return "No se pudo realizar la conexion con "+webPage;
		}
	}

	private String getABC() {
		Document document;
		String webPage = "https://www.abc.es";
		try {
			document = Jsoup.connect(webPage).get();
			Elements palabra = document.getElementsByClass("titular");
			return palabra.get(0).text();
		} catch (IOException e) {
			return "No se pudo realizar la conexion con "+webPage;
		}
	}

	private String getCNN() {
		Document document;
		String webPage = "https://cnnespanol.cnn.com/?hpt=header_edition-picker";
		try {
			document = Jsoup.connect(webPage).get();
			Elements palabra = document.getElementsByClass("news__title");
			return palabra.get(0).text();
		} catch (IOException e) {
			return "No se pudo realizar la conexion con "+webPage;
		}
	}

	private String getTHEGUARDIAN() {
		Document document;
		String webPage = "https://www.theguardian.com/international";
		try {
			document = Jsoup.connect(webPage).get();
			Elements palabra = document.getElementsByClass("u-faux-block-link__overlay js-headline-text");
			return palabra.get(0).text();
		} catch (IOException e) {
			return "No se pudo realizar la conexion con "+webPage;
		}
	}

	private String getNYTIMES() {
		Document document;
		String webPage = "https://www.nytimes.com/es/";
		try {
			document = Jsoup.connect(webPage).get();
			Elements palabra = document.getElementsByClass("css-byk1jx e4e4i5l1");
			return palabra.get(0).text();
		} catch (IOException e) {
			return "No se pudo realizar la conexion con "+webPage;
		}
	}

	private String getBBC() {
		Document document;
		String webPage = "https://www.bbc.com/mundo";
		try {
			document = Jsoup.connect(webPage).get();
			Elements palabra = document.getElementsByClass("css-wla3o-Link evnt13t2");
			return palabra.get(0).text();
		} catch (IOException e) {
			return "No se pudo realizar la conexion con "+webPage;
		}
	}

	private String getCINCODIAS() {
		Document document;
		String webPage = "https://cincodias.elpais.com/";
		try {
			document = Jsoup.connect(webPage).get();
			Elements palabra = document.getElementsByClass("articulo-texto");
			return palabra.get(0).text();
		} catch (IOException e) {
			return "No se pudo realizar la conexion con "+webPage;
		}
	}

	private String getMEGABOLSA() {
		Document document;
		String webPage = "https://www.megabolsa.com/";
		try {
			document = Jsoup.connect(webPage).get();
			Elements palabra = document.getElementsByClass("entry-title");
			return palabra.get(0).text();
		} catch (IOException e) {
			return "No se pudo realizar la conexion con "+webPage;
		}
	}

	private String getEXPANSION() {
		Document document;
		String webPage = "https://www.expansion.com/";
		try {
			document = Jsoup.connect(webPage).get();
			Elements palabra = document.getElementsByClass("ue-c-cover-content__link");
			return palabra.get(0).text();
		} catch (IOException e) {
			return "No se pudo realizar la conexion con "+webPage;
		}
	}

	private String getELECONOMISTA() {
		Document document;
		String webPage = "https://www.eleconomista.es/";
		try {
			document = Jsoup.connect(webPage).get();
			Elements palabra = document.getElementsByClass("h1");
			return palabra.get(0).text();
		} catch (IOException e) {
			return "No se pudo realizar la conexion con "+webPage;
		}
	}

	private String getSPORT() {
		Document document;
		String webPage = "https://www.sport.es/es/";
		try {
			document = Jsoup.connect(webPage).get();
			Elements palabra = document.getElementsByClass("type-2");
			return palabra.get(0).text();
		} catch (IOException e) {
			return "No se pudo realizar la conexion con "+webPage;
		}
	}

	private String getMARCA() {
		Document document;
		String webPage = "https://www.marca.com/";
		try {
			document = Jsoup.connect(webPage).get();
			Elements palabra = document.getElementsByClass("mod-header");
			return palabra.get(0).getElementsByClass("mod-title").text();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "No se pudo realizar la conexion con "+webPage;
		}
	}
	
	private String getMUNDODEPORTIVO() {
		Document document;
		String webPage = "https://www.mundodeportivo.com/";
		try {
			document = Jsoup.connect(webPage).get();
			Elements palabra = document.getElementsByClass("story-header-title");
			return palabra.get(0).getElementsByClass("story-header-title-link article-link").text();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "No se pudo realizar la conexion con "+webPage;
		}
	}
	
	private String getAS() {
		Document document;
		String webPage = "https://as.com/?omnaut=1";
		try {
			document = Jsoup.connect(webPage).get();
			Elements palabra = document.getElementsByClass("pntc-content");
			return palabra.get(0).getElementsByClass("title").text();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "No se pudo realizar la conexion con "+webPage;
		}
	}
}
