package fileManager;

import java.io.IOException;
import java.util.HashMap;

import logic.Configuracion;
import logic.Fuente;
import logic.Perodico;

/**
 * @author vtienza
 *
 */
public class FicheroConfiguracion extends Fichero {

	public FicheroConfiguracion(String nombreFichero) {
		super(nombreFichero);
	}
	
	public void escribir(int idUsuario, Configuracion configuracion) {
		try {
			//format configuracion
			String lineaConfiguracion = idUsuario + "\n";
			lineaConfiguracion += configuracion.cantidadFuentes() + "\n";
			lineaConfiguracion += configuracion.toString();
			super.ficheroW.write(lineaConfiguracion);
		} catch (IOException e) {
			//TODO ventana de error
		}
	}
	
	public void sobrescribir(int idUsuario,Configuracion nuevaconfiguracion) {
		HashMap<Integer, Configuracion> configuraciones = new HashMap<Integer, Configuracion>();
			//leer todo el fichero y arrayList de toda la configuracion menos la del usuario
		while (super.ficheroR.hasNextInt()) {
			Configuracion configuracion;
			Integer user = super.ficheroR.nextInt();
			if (user != idUsuario) {
				configuracion = new Configuracion();
				int numNoticias = super.ficheroR.nextInt();
				for (int i = 0; i < numNoticias; i++)
					configuracion.addFuente(new Fuente(Perodico.valueOf(super.ficheroR.nextLine())));
				configuraciones.put(user,configuracion);
			}
			else {
				int numNoticias = super.ficheroR.nextInt();
				for (int i = 0; i < numNoticias; i++)
					super.ficheroR.nextLine();
			}
		}
		super.vaciar();
		
		configuraciones.put(idUsuario, nuevaconfiguracion);
		
		for (Integer id : configuraciones.keySet()) 
			escribir(id, configuraciones.get(id));
	}
	
	public Configuracion leer(int idUsuario) {
		Configuracion configuracion;
		if (super.ficheroR.hasNextInt()) {
			if (super.ficheroR.nextInt() == idUsuario) {
				configuracion = new Configuracion();
				int numNoticias = super.ficheroR.nextInt();
				super.ficheroR.nextLine();
				for (int i = 0; i < numNoticias; i++) {
					String data = super.ficheroR.nextLine();
					configuracion.addFuente(new Fuente(Perodico.valueOf(data)));
				}
				return configuracion;
			}
			else {
				int numNoticias = super.ficheroR.nextInt();
				for (int i = 0; i < numNoticias; i++);
					super.ficheroR.nextLine();
				return leer(idUsuario);
			}
		}
		return null;
	}
	
	public boolean existeUsuario(int idUsuario) {
		
		if (leer(idUsuario) != null)
			return true;
		else
			return false;
		
	}

}
