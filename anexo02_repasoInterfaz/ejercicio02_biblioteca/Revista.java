/**
 * 
 */
package ejercicio02_biblioteca;

/**
 * @author vtienza
 *
 */
public class Revista extends Documento{
	private int numero;

	/**
	 * @param codigo
	 * @param titulo
	 * @param añoDePublicacion
	 * @param numero
	 */
	public Revista(int codigo, String titulo, int añoDePublicacion, int numero) {
		super(codigo, titulo, añoDePublicacion);
		this.numero = numero;
	}

	/**
	 * @return the numero
	 */
	public int getNumero() {
		return numero;
	}

	/**
	 * @param numero the numero to set
	 */

	@Override
	public String toString() {
		return "Revista [ numero=" + numero + super.toString() + " ]";
	}

}
