/**
 * 
 */
package ejercicio02_biblioteca;

/**
 * @author vtienza
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Libro elCid = new Libro(02556, "El Cid Campeador", 1564);
		Revista batman = new Revista(02336, "DC BATMAN ARKAN", 1999, 200);
		
		elCid.devolver();
		elCid.prestar();
		System.out.println( elCid.toString());
		System.out.println( batman.toString());

	}

}
