/**
 * 
 */
package ejercicio02_biblioteca;

/**
 * @author vtienza
 *
 */
public class Libro extends Documento implements Prestable {
	private boolean prestado;

	/**
	 * @param codigo
	 * @param titulo
	 * @param añoDePublicacion
	 */
	public Libro(int codigo, String titulo, int añoDePublicacion) {
		super(codigo, titulo, añoDePublicacion);
		prestado = false;
	}

	/**
	 * @return the prestado
	 */
	public boolean prestado() {
		return prestado;
	}
	public void devolver() {
		if (prestado)
			prestado = false;
		else
			System.err.println("El Libro no ha sido prestado");
	}
	
	public void prestar() {
		if (!prestado)
			prestado = true;
		else
			System.err.println("El Libro ya ha sido prestado");
	}

	@Override
	public String toString() {
		return "Libros [ prestado=" + prestado + super.toString()+ " ]";
	}
	
	

}
