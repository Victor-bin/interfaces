/**
 * 
 */
package ejercicio02_biblioteca;

/**
 * @author vtienza
 *
 */
public interface Prestable {
	
	/**
	 * El objeto se presta
	 */
	public void prestar();
		
	/**
	 * El objeto se devuelve
	 */
	public void devolver();
	
	/**
	 * Mira si el objeto esta siendo prestado
	 * @return un boolean con el estado del libro (prestado o no)
	 */
	public boolean prestado();
}
