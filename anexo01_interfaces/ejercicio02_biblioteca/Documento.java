/**
 * 
 */
package ejercicio02_biblioteca;

/**
 * @author vtienza
 *
 */
public class Documento {
	private int codigo;
	private String titulo;
	private int añoDePublicacion;
		
	/**
	 * @param codigo
	 * @param titulo
	 * @param añoDePublicacion
	 */
	public Documento(int codigo, String titulo, int añoDePublicacion) {
		super();
		this.codigo = codigo;
		this.titulo = titulo;
		this.añoDePublicacion = añoDePublicacion;
	}

	/**
	 * @return the codigo
	 */
	public int getCodigo() {
		return codigo;
	}

	/**
	 * @return the titulo
	 */
	public String getTitulo() {
		return titulo;
	}

	/**
	 * @return the añoDePublicacion
	 */
	public int getAñoDePublicacion() {
		return añoDePublicacion;
	}

	@Override
	public String toString() {
		return ", codigo=" + codigo + ", titulo=" + titulo + ", añoDePublicacion=" + añoDePublicacion;
	}
}
